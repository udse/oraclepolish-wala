java -cp build:\
lib/guava-14.0.1.jar:\
lib/jackson-annotations-2.2.2.jar:\
lib/jackson-core-2.2.2.jar:\
lib/jackson-databind-2.2.2.jar:\
lib/jopt-simple-4.5.jar \
edu.udel.testassist.analyzer.Main \
-a ~/Projects/testassist/subjects/Sudoku/build/app \
-a ~/Projects/testassist/subjects/Sudoku/build/test/original \
-e ~/Projects/testassist/subjects/Sudoku/lib/junit-4.11.jar \
-o sudoku.out
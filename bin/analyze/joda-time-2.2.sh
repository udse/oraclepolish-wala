java -cp build:lib/guava-14.0.1.jar:lib/jackson-annotations-2.2.2.jar:lib/jackson-core-2.2.2.jar:lib/jackson-databind-2.2.2.jar:lib/jopt-simple-4.5.jar edu.udel.testassist.analyzer.Main \
-a ~/Projects/testassist/subjects/joda-time-2.2/target/classes \
-a ~/Projects/testassist/subjects/joda-time-2.2/target/test-classes \
-e ~/Projects/testassist/subjects/joda-time-2.2/lib/joda-convert-1.2.jar \
-e ~/Projects/testassist/subjects/joda-time-2.2/lib/junit-3.8.2.jar \
-o joda-time-2.2.conf
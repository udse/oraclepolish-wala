#!/bin/bash

SUBJECTS_HOME=$HOME/Projects/testassist/subjects
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

java -cp $DIR/../../../dist/testassist.jar edu.udel.testassist.analyzer.Main \
-a $SUBJECTS_HOME/apache-xml-security/build/app \
-a $SUBJECTS_HOME/apache-xml-security/build/test/original \
-e $SUBJECTS_HOME/apache-xml-security/data \
-e $SUBJECTS_HOME/lib/hamcrest-all-1.3.jar \
-e $SUBJECTS_HOME/lib/junit-4.11.jar \
-e $SUBJECTS_HOME/apache-xml-security/lib/commons-logging-api.jar \
-e $SUBJECTS_HOME/apache-xml-security/lib/commons-logging.jar \
-e $SUBJECTS_HOME/apache-xml-security/lib/xalan.jar \
-e $SUBJECTS_HOME/apache-xml-security/lib/rt.jar \
-e $SUBJECTS_HOME/apache-xml-security/lib/bc-jce-jdk13-114.jar \
-e $SUBJECTS_HOME/apache-xml-security/lib/ant.jar \
-e $SUBJECTS_HOME/apache-xml-security/lib/xercesImpl.jar \
-e $SUBJECTS_HOME/apache-xml-security/lib/log4j-1.2.8.jar \
-e $SUBJECTS_HOME/apache-xml-security/lib/junitSIR.jar \
-e $SUBJECTS_HOME/apache-xml-security/lib/xml-apis.jar \
-e $SUBJECTS_HOME/apache-xml-security/lib/xmlParserAPIs.jar \
-e $SUBJECTS_HOME/apache-xml-security/lib/stylebook-1.0-b3_xalan-2.jar \
-e $SUBJECTS_HOME/apache-xml-security/lib/style-apachexml.jar \
-e $DIR/../../../build \
-o $SUBJECTS_HOME/apache-xml-security/apache-xml-security_original.conf

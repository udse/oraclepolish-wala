#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "Barbecue"
$DIR/Barbecue_original.sh &> Barbecue.out

echo "CommissionEmployee"
$DIR/CommissionEmployee_original.sh &> CommissionEmployee.out

echo "DataStructures"
$DIR/DataStructures_original.sh &> DataStructures.out

echo "Employee"
$DIR/Employee_original.sh &> Employee.out

echo "LoopFinder"
$DIR/LoopFinder_original.sh &> LoopFinder.out

echo "Point"
$DIR/Point_original.sh &> Point.out

echo "ReductionAndPrioritization"
$DIR/ReductionAndPrioritization_original.sh &> ReductionAndPrioritization.out

echo "Sudoku"
$DIR/Sudoku_original.sh &> Sudoku.out

echo "apache-xml-security"
$DIR/apache-xml-security_original.sh &> apache-xml-security.out

echo "commons-beanutils"
$DIR/commons-beanutils_original.sh &> commons-beanutils.out

echo "commons-cli"
$DIR/commons-cli_original.sh &> commons-cli.out

echo "commons-collections"
$DIR/commons-collections_original.sh &> commons-collections.out

echo "commons-io"
$DIR/commons-io_original.sh &> commons-io.out

echo "commons-lang"
$DIR/commons-lang_original.sh &> commons-lang.out

echo "commons-math"
$DIR/commons-math_original.sh &> commons-math.out

echo "JDepend"
$DIR/JDepend_original.sh &> JDepend.out

echo "joda-convert"
$DIR/joda-convert-1.2_original.sh &> joda-convert-1.2.out

echo "JodaTime"
$DIR/JodaTime_original.sh &> JodaTime.out

echo "jtopas"
$DIR/jtopas_original.sh &> jtopas.out

echo "jfreechart-1.0.15"
$DIR/jfreechart-1.0.15_original.sh  &> jfreechart-1.0.15.out

echo "joda-time-2.2"
$DIR/joda-time-2.2_original.sh  &> joda-time-2.2.out

echo "pmd-5.0.4"
$DIR/pmd-5.0.4_original.sh  &> pmd-5.0.4.out

echo "gson-2.2.4"
$DIR/gson-2.2.4_original.sh  &> gson-2.2.4.out




#!/bin/bash

SUBJECTS_HOME=$HOME/Projects/testassist/subjects
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

java -cp $DIR/../../../dist/testassist.jar edu.udel.testassist.analyzer.Main \
-a $SUBJECTS_HOME/jfreechart-1.0.15/build/app \
-a $SUBJECTS_HOME/jfreechart-1.0.15/build/experimental \
-a $SUBJECTS_HOME/jfreechart-1.0.15/build/swt \
-a $SUBJECTS_HOME/jfreechart-1.0.15/build/test/original \
-e $SUBJECTS_HOME/lib/hamcrest-all-1.3.jar \
-e $SUBJECTS_HOME/lib/junit-4.11.jar \
-e $SUBJECTS_HOME/jfreechart-1.0.15/lib/iText-2.1.5.jar \
-e $SUBJECTS_HOME/jfreechart-1.0.15/lib/jcommon-1.0.18.jar \
-e $SUBJECTS_HOME/jfreechart-1.0.15/lib/servlet.jar \
-e $SUBJECTS_HOME/jfreechart-1.0.15/lib/swtgraphics2d.jar \
-e $SUBJECTS_HOME/jfreechart-1.0.15/lib/org.eclipse.swt.gtk.linux.x86_64_3.100.1.v4234e.jar \
-e $DIR/../../../build \
-o $SUBJECTS_HOME/jfreechart-1.0.15/jfreechart-1.0.15_original.conf

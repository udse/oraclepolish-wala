#!/bin/bash

SUBJECTS_HOME=$HOME/Projects/testassist/subjects
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

java -cp $DIR/../../../dist/testassist.jar edu.udel.testassist.analyzer.Main \
-a $SUBJECTS_HOME/joda-time-2.2/build/app \
-a $SUBJECTS_HOME/joda-time-2.2/build/test/original \
-e $SUBJECTS_HOME/joda-time-2.2/lib/joda-convert-1.2.jar \
-e $SUBJECTS_HOME/lib/hamcrest-all-1.3.jar \
-e $SUBJECTS_HOME/lib/junit-4.11.jar \
-e $DIR/../../../build \
-o $SUBJECTS_HOME/joda-time-2.2/joda-time-2.2_original.conf

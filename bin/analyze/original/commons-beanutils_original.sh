#!/bin/bash

SUBJECTS_HOME=$HOME/Projects/testassist/subjects
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

java -cp $DIR/../../../dist/testassist.jar edu.udel.testassist.analyzer.Main \
-a $SUBJECTS_HOME/commons-beanutils/build/app \
-a $SUBJECTS_HOME/commons-beanutils/build/test/original \
-e $SUBJECTS_HOME/lib/hamcrest-all-1.3.jar \
-e $SUBJECTS_HOME/lib/junit-4.11.jar \
-e $SUBJECTS_HOME/commons-beanutils/lib/commons-collections-3.2.jar \
-e $SUBJECTS_HOME/commons-beanutils/lib/commons-collections-testframework-3.2.jar \
-e $SUBJECTS_HOME/commons-beanutils/lib/commons-logging-1.1.1.jar \
-e $DIR/../../../build \
-o $SUBJECTS_HOME/commons-beanutils/commons-beanutils_original.conf

#!/bin/bash

SUBJECTS_HOME=$HOME/Projects/testassist/subjects

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

java -cp $DIR/../../../dist/testassist.jar edu.udel.testassist.analyzer.Main \
-a $SUBJECTS_HOME/Sudoku/build/app \
-a $SUBJECTS_HOME/Sudoku/build/test/original \
-e $DIR/../../../build \
-e $SUBJECTS_HOME/lib/hamcrest-all-1.3.jar \
-e $SUBJECTS_HOME/lib/junit-4.11.jar \
-o $SUBJECTS_HOME/Sudoku/Sudoku_original.conf

#!/bin/bash

SUBJECTS_HOME=$HOME/Projects/testassist/subjects
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

java -cp $DIR/../../../dist/testassist.jar edu.udel.testassist.analyzer.Main \
-a $SUBJECTS_HOME/Barbecue/build/app \
-a $SUBJECTS_HOME/Barbecue/build/test/original \
-e $SUBJECTS_HOME/Barbecue/lib/jdom.jar \
-e $SUBJECTS_HOME/Barbecue/lib/portlet-api-2.0-prerelease.jar \
-e $SUBJECTS_HOME/Barbecue/lib/servletapi-2.2.jar \
-e $SUBJECTS_HOME/lib/hamcrest-all-1.3.jar \
-e $SUBJECTS_HOME/lib/junit-4.11.jar \
-e $DIR/../../../build \
-o $SUBJECTS_HOME/Barbecue/Barbecue_original.conf

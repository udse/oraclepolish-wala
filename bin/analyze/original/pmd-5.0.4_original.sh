#!/bin/bash

SUBJECTS_HOME=$HOME/Projects/testassist/subjects
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

java -cp $DIR/../../../dist/testassist.jar edu.udel.testassist.analyzer.Main \
-a $SUBJECTS_HOME/pmd-5.0.4/build/app \
-a $SUBJECTS_HOME/pmd-5.0.4/build/test/original \
-e $SUBJECTS_HOME/lib/hamcrest-all-1.3.jar \
-e $SUBJECTS_HOME/lib/junit-4.11.jar \
-e $SUBJECTS_HOME/pmd-5.0.4/lib/ant-1.8.1.jar \
-e $SUBJECTS_HOME/pmd-5.0.4/lib/ant-launcher-1.8.1.jar \
-e $SUBJECTS_HOME/pmd-5.0.4/lib/ant-testutil-1.7.1.jar \
-e $SUBJECTS_HOME/pmd-5.0.4/lib/asm-3.2.jar \
-e $SUBJECTS_HOME/pmd-5.0.4/lib/commons-io-2.2.jar \
-e $SUBJECTS_HOME/pmd-5.0.4/lib/dom4j-1.6.1.jar \
-e $SUBJECTS_HOME/pmd-5.0.4/lib/javacc-4.1.jar \
-e $SUBJECTS_HOME/pmd-5.0.4/lib/jaxen-1.1.1.jar \
-e $SUBJECTS_HOME/pmd-5.0.4/lib/jcommander-1.27.jar \
-e $SUBJECTS_HOME/pmd-5.0.4/lib/jdom-1.0.jar \
-e $SUBJECTS_HOME/pmd-5.0.4/lib/rhino-1.7R3.jar \
-e $SUBJECTS_HOME/pmd-5.0.4/lib/saxon-9.1.0.8.jar \
-e $SUBJECTS_HOME/pmd-5.0.4/lib/saxon-9.1.0.8-dom.jar \
-e $SUBJECTS_HOME/pmd-5.0.4/lib/xercesImpl-2.9.1.jar \
-e $SUBJECTS_HOME/pmd-5.0.4/lib/xml-apis-1.3.02.jar \
-e $SUBJECTS_HOME/pmd-5.0.4/lib/xmlParserAPIs-2.6.2.jar \
-e $SUBJECTS_HOME/pmd-5.0.4/lib/xom-1.0.jar \
-e $DIR/../../../build \
-o $SUBJECTS_HOME/pmd-5.0.4/pmd-5.0.4_original.conf

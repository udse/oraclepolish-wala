#!/bin/bash

SUBJECTS_HOME=$HOME/Projects/testassist/subjects
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

java -cp $DIR/../../../dist/testassist.jar edu.udel.testassist.analyzer.Main \
-a $SUBJECTS_HOME/commons-collections/build/app \
-a $SUBJECTS_HOME/commons-collections/build/test/original \
-e $SUBJECTS_HOME/lib/hamcrest-all-1.3.jar \
-e $SUBJECTS_HOME/lib/junit-4.11.jar \
-e $DIR/../../../build \
-o $SUBJECTS_HOME/commons-collections/commons-collections_original.conf

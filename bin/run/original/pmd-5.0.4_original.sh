#!/bin/bash

SUBJECTS_HOME=$HOME/Projects/testassist/subjects
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

java -cp $DIR/../../../dist/testassist.jar edu.udel.testassist.visualizer.Main \
-c $SUBJECTS_HOME/pmd-5.0.4/pmd-5.0.4_original.conf \
-o $SUBJECTS_HOME/pmd-5.0.4/result/original

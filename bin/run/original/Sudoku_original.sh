#!/bin/bash

SUBJECTS_HOME=$HOME/Projects/testassist/subjects
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"ls

java -cp $DIR/../../../dist/testassist.jar edu.udel.testassist.visualizer.Main \
-c $SUBJECTS_HOME/Sudoku/Sudoku_original.conf \
-o $SUBJECTS_HOME/Sudoku/result/original

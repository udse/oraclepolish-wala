#!/bin/bash

SUBJECTS_HOME=$HOME/Projects/testassist/subjects
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


java -cp $DIR/../../../dist/testassist.jar edu.udel.testassist.visualizer.Main \
-c $SUBJECTS_HOME/jfreechart-1.0.15/jfreechart-1.0.15_original.conf \
-o $SUBJECTS_HOME/jfreechart-1.0.15/result/original \

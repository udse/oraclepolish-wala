package edu.udel.testassist.analyzer.utils;

import org.jgraph.graph.DefaultEdge;

public class TAEdge extends DefaultEdge {
	private TANode source;
	private TANode target;
	
	public TAEdge(TANode source , TANode target){
		super();
		this.source = source;
		this.target = target;
		this.setSource(source);
		this.setTarget(target);
	}
	
	@Override
	public TANode getSource(){
		return this.source;
	}

	@Override
	public TANode getTarget(){
		return this.target;
	}
}

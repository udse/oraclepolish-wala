package edu.udel.testassist.analyzer.utils;

public abstract class TANode {

	@Override
	public boolean equals(Object that) {
		if (that instanceof TANode) {
			TANode dat = (TANode) that;
			return dat.hashCode() == this.hashCode();
		}

		return false;
	}

}

package edu.udel.testassist.analyzer.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.ibm.wala.classLoader.IBytecodeMethod;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.classLoader.ShrikeCTMethod;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.types.ClassLoaderReference;
import com.ibm.wala.types.TypeName;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.types.annotations.Annotation;
import com.ibm.wala.util.strings.Atom;

public class Util {

	private final static TypeName OrgJUnitTestCaseName = TypeName.string2TypeName("Ljunit/framework/TestCase");
	private final static TypeReference OrgJUnitTestCase = TypeReference.findOrCreate(ClassLoaderReference.Application, OrgJUnitTestCaseName);

	private final static TypeName OrgJUnitTestName = TypeName.string2TypeName("Lorg/junit/Test");
	private final static TypeReference OrgJUnitTest = TypeReference.findOrCreate(ClassLoaderReference.Application, OrgJUnitTestName);

	private final static TypeName OrgJUnitBeforeName = TypeName.string2TypeName("Lorg/junit/Before");
	private final static TypeReference OrgJUnitBefore = TypeReference.findOrCreate(ClassLoaderReference.Application, OrgJUnitBeforeName);

	private final static TypeName OrgJUnitAfterName = TypeName.string2TypeName("Lorg/junit/After");
	private final static TypeReference OrgJUnitAfter = TypeReference.findOrCreate(ClassLoaderReference.Application, OrgJUnitAfterName);

	private static Atom setUp = Atom.findOrCreateAsciiAtom("setUp");
	private static Atom tearDown = Atom.findOrCreateAsciiAtom("tearDown");

	public static Predicate<IClass> isApplicationClass = new Predicate<IClass>() {
		public boolean apply(IClass cls) {
			AnalysisScope analysisScope = cls.getClassHierarchy().getScope();
			return analysisScope.getApplicationLoader().equals(cls.getClassLoader().getReference());
		}
	};

	public static Predicate<IClass> isPrimordialClass = new Predicate<IClass>() {
		public boolean apply(IClass cls) {
			AnalysisScope analysisScope = cls.getClassHierarchy().getScope();
			return analysisScope.getApplicationLoader().equals(cls.getClassLoader().getReference());
		}
	};

	public static Predicate<IClass> isExtensionClass = new Predicate<IClass>() {
		public boolean apply(IClass cls) {
			AnalysisScope analysisScope = cls.getClassHierarchy().getScope();
			return analysisScope.getApplicationLoader().equals(cls.getClassLoader().getReference());
		}
	};

	public static Predicate<IClass> isSubclassOfTestCase = new Predicate<IClass>() {
		public boolean apply(IClass cls) {
			IClassHierarchy classHierarchy = cls.getClassHierarchy();

			IClass testCaseClass = classHierarchy.lookupClass(OrgJUnitTestCase);

			return classHierarchy.isSubclassOf(cls, testCaseClass);
		}
	};

	private static class HasAnnotation implements Predicate<IMethod> {

		private final Predicate<Annotation> isAnnotation;

		public HasAnnotation(final TypeReference annotationReference) {
			this.isAnnotation = new Predicate<Annotation>() {
				public boolean apply(Annotation annotation) {
					return annotation.getType().equals(annotationReference);
				}
			};
		}

		public boolean apply(IMethod method) {

			if (!(method instanceof ShrikeCTMethod))
				return false;

			ShrikeCTMethod shrikeCTMethod = (ShrikeCTMethod) method;
			Collection<Annotation> annotations = new HashSet<>();

			try {
				annotations.addAll(shrikeCTMethod.getRuntimeVisibleAnnotations());
			} catch (InvalidClassFileException e) {
				e.printStackTrace();
			}

			return Iterables.any(annotations, isAnnotation);
		}
	}

	public static Predicate<IMethod> hasTestAnnotation = new HasAnnotation(OrgJUnitTest);
	public static Predicate<IMethod> hasBeforeAnnotation = new HasAnnotation(OrgJUnitBefore);
	public static Predicate<IMethod> hasAfterAnnotation = new HasAnnotation(OrgJUnitAfter);

	// Predicate to check whether a method should be considered a test
	public static Predicate<IMethod> isTestMethod = new Predicate<IMethod>() {
		public boolean apply(IMethod method) {

			// JUnit 4, test methods are annotated with @Test
			if (hasTestAnnotation.apply(method)) {
				return true;
			}

			// in JUnit 3, test methods are methods in subclasses of TestCase that start with "test"
			return isSubclassOfTestCase.apply(method.getDeclaringClass()) && method.getName().toString().startsWith("test");
		}
	};

	public static Predicate<IMethod> isSetupMethod = new Predicate<IMethod>() {
		public boolean apply(IMethod method) {

			// JUnit 4, setUp methods are annotated with @Before
			if (hasBeforeAnnotation.apply(method)) {
				return true;
			}

			// in JUnit 3, setUp methods are methods in subclasses of TestCase named "setUp"
			return isSubclassOfTestCase.apply(method.getDeclaringClass()) && method.getName().equals(setUp);
		}
	};

	public static Predicate<IMethod> isTearDownMethod = new Predicate<IMethod>() {
		public boolean apply(IMethod method) {

			// JUnit 4 tearDown methods are annotated with @After
			if (hasAfterAnnotation.apply(method)) {
				return true;
			}

			// in JUnit 3, tearDown methods are methods in subclasses of TestCase named "tearDown"
			return isSubclassOfTestCase.apply(method.getDeclaringClass()) && method.getName().equals(tearDown);
		}
	};

	@SuppressWarnings("unchecked")
	public static Predicate<IMethod> isJUnitMethod = Predicates.or(isTestMethod, isSetupMethod, isTearDownMethod);

//	private static Collection<String> assertMethodNames = ImmutableSet.of(
//			"assertArrayEquals",
//			"assertEquals",
//			"assertFalse",
//			"assertNotNull",
//			"assertNotSame",
//			"assertNull",
//			"assertSame",
//			"assertTrue"
//			);

//	public static Predicate<SSAInvokeInstruction> isAssertInvocation = new Predicate<SSAInvokeInstruction>() {
//
//		public boolean apply(SSAInvokeInstruction invoke) {
//
//			String name = invoke.getDeclaredTarget().getName().toString();
//
//			return assertMethodNames.contains(name);
//
//		}
//	};

	public static Predicate<SSAInvokeInstruction> isAssertInvocation = new Predicate<SSAInvokeInstruction>() {

		public boolean apply(SSAInvokeInstruction invoke) {

			String name = invoke.getDeclaredTarget().getName().toString();

			return name.startsWith("assert");

		}
	};
	
	public static <T> Set<T> foldLeft(final ArrayList<T> lst, 
									Function<T, Function<Set<T>, Set<T>>> f, Set<T> accu){
		if (lst.isEmpty()) return accu;
		T next = lst.remove(0);
		return foldLeft(lst, f, f.apply(next).apply(accu));
	}
	
	public static Function<IBytecodeMethod, Collection<Integer>> lineNumbersOfMethod = new Function<IBytecodeMethod, Collection<Integer>> () {
		public Collection<Integer> apply(IBytecodeMethod method) {
			ImmutableSet.Builder<Integer> builder = ImmutableSet.builder();
			
			try {
				for(int i = 0; i < method.getInstructions().length; i++) {
					int bcIndex = method.getBytecodeIndex(i);
					builder.add(method.getLineNumber(bcIndex));
				}
			} catch (InvalidClassFileException e) {
				e.printStackTrace();
			}
			
			return builder.build();
		}
	};

}
package edu.udel.testassist.analyzer.utils;

public class VariableNode extends TANode {
	
	private int v;

	public VariableNode(int v) {
		this.v = v;
	}
	
	@Override
	public int hashCode(){
		return v;
	}
	
	@Override
	public String toString(){
		return "v" + v;
	}

}

package edu.udel.testassist.analyzer.utils;

import com.ibm.wala.types.FieldReference;

public class FieldNode extends TANode {
	
	private FieldReference fref;
	private int v;
	
	public FieldNode(int v, FieldReference fref) {
		this.v = v;
		this.fref = fref;
	}
	
	@Override
	public int hashCode(){
		return v * 19716 + fref.hashCode();
	}
	
	@Override
	public String toString(){
		return String.format("v%d#s", v, fref.getSignature());
	}

}

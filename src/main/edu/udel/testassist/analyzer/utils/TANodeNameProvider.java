package edu.udel.testassist.analyzer.utils;

import org.jgrapht.ext.VertexNameProvider;

public class TANodeNameProvider implements VertexNameProvider<TANode>{
	
	public String getVertexName(TANode node){
		return node.toString();
	}

}

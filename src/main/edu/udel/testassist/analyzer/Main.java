package edu.udel.testassist.analyzer;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Charsets;
import com.google.common.base.Function;
import com.google.common.base.Splitter;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.common.collect.Iterables;
import com.google.common.io.Resources;
import com.ibm.wala.classLoader.BinaryDirectoryTreeModule;
import com.ibm.wala.classLoader.ClassFileModule;
import com.ibm.wala.classLoader.JarFileModule;
import com.ibm.wala.classLoader.Module;
import com.ibm.wala.classLoader.NestedJarFileModule;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.cha.ClassHierarchy;
import com.ibm.wala.ipa.cha.ClassHierarchyException;
import com.ibm.wala.properties.WalaProperties;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.ibm.wala.types.ClassLoaderReference;
import com.ibm.wala.util.CancelException;
import com.ibm.wala.util.config.FileOfClasses;
import com.ibm.wala.util.strings.Atom;

import edu.udel.testassist.configuration.Configuration;
import edu.udel.testassist.configuration.ControlFlowInfo;
import edu.udel.testassist.configuration.TestInfo;

public class Main {

	private static AnalysisScope createAnalysisScope() {

		AnalysisScope analysisScope = AnalysisScope.createJavaAnalysisScope();

		Splitter splitter = Splitter.on(",").trimResults();
		try {
			for (String line : Resources.readLines(Resources.getResource("primordial.txt"), Charsets.UTF_8)) {
				String[] toks = Iterables.toArray(splitter.split(line), String.class);

				Atom loaderName = Atom.findOrCreateUnicodeAtom(toks[0]);
				ClassLoaderReference loader = analysisScope.getLoader(loaderName);

				switch (toks[2]) {
				case "stdlib":
					for (String stdlib : WalaProperties.getJ2SEJarFiles()) {
						analysisScope.addToScope(loader, new JarFile(stdlib));
					}
					break;
				case "jarfile":
					URL url = Resources.getResource(toks[3]);
					if (url.getProtocol().equals("jar")) {
						JarURLConnection jc = (JarURLConnection) url.openConnection();
						JarFile f = jc.getJarFile();
						JarEntry entry = jc.getJarEntry();
						JarFileModule parent = new JarFileModule(f);
						analysisScope.addToScope(loader, new NestedJarFileModule(parent, entry));
					}
					else {
						analysisScope.addToScope(loader, new JarFileModule(new JarFile(new File(url.getPath()).toURI().getPath(), false)));
					}
					break;
				default:
					break;
				}

			}

			analysisScope.setExclusions(new FileOfClasses(Resources.newInputStreamSupplier(Resources.getResource("Java60RegressionExclusions.txt"))
					.getInput()));
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}

		return analysisScope;

	}

	private static void addToScope(final AnalysisScope analysisScope, final ClassLoaderReference classLoaderReference, final File file) {

		try {
			Module module = null;

			if (file.isDirectory()) {
				module = new BinaryDirectoryTreeModule(file);
			}
			else if (file.getName().endsWith(".jar")) {
				module = new JarFileModule(new JarFile(file));
			}
			else {
				module = new ClassFileModule(file);
			}

			analysisScope.addToScope(classLoaderReference, module);
		} catch (IOException | InvalidClassFileException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws CancelException, IOException {

		OptionParser parser = new OptionParser();

		OptionSpec<File> applicationOption = parser.accepts("application").withRequiredArg().ofType(File.class).required();
		OptionSpec<File> extensionOption = parser.accepts("extension").withRequiredArg().ofType(File.class);
		OptionSpec<PrintWriter> output = parser.accepts("output").withRequiredArg().ofType(PrintWriter.class)
				.defaultsTo(new PrintWriter(System.out));

		OptionSet options = parser.parse(args);

		AnalysisScope analysisScope = createAnalysisScope();

		for (File applicationModule : options.valuesOf(applicationOption)) {
			addToScope(analysisScope, analysisScope.getApplicationLoader(), applicationModule);
		}

		for (File extensionModule : options.valuesOf(extensionOption)) {
			addToScope(analysisScope, analysisScope.getExtensionLoader(), extensionModule);
		}

		ClassHierarchy classHierarchy = null;

		try {
			classHierarchy = ClassHierarchy.make(analysisScope);
			System.out.println(analysisScope);
		} catch (ClassHierarchyException e) {
			e.printStackTrace();
			System.exit(-1);
		}

		Function<File, String> ABSOLUTE_PATH = new Function<File, String>() {
			public String apply(File f) {
				return f.getAbsolutePath();
			}
		};
		
		List<String> classpath = FluentIterable.from(Iterables.concat(options.valuesOf(applicationOption), options.valuesOf(extensionOption)))
				.transform(ABSOLUTE_PATH)
				.toList();

		Builder<String> builder = ImmutableList.builder();
		List<String> cp = builder
				.add("/home/huoc/Projects/testassist/testassist-jpf/lib/jackson-annotations-2.2.2.jar")
				.addAll(classpath).build();

		AnalysisScope ana = AnalysisScope.createJavaAnalysisScope();
		for (File applicationModule : options.valuesOf(applicationOption)) {
			addToScope(analysisScope, analysisScope.getApplicationLoader(), applicationModule);
		}

		Future<List<TestInfo>> result = new Analyzer().process(classHierarchy);

		List<TestInfo> tests = new LinkedList<>();
		try {
			tests.addAll(result.get());
			System.out.println("# Tests:" + tests.size());
		} catch (InterruptedException | ExecutionException e1) {
			e1.printStackTrace();
		} catch (StackOverflowError e) {
			e.printStackTrace();
			System.exit(1);
		}

		Configuration c = new Configuration(classpath, tests);

		ObjectMapper mapper = new ObjectMapper();

		try {
			mapper.writeValue(options.valueOf(output), c);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

//		Future<List<ControlFlowInfo>> cfresults = new CFAnalyzer().process(classHierarchy);
//		List<ControlFlowInfo> controlinfos = new LinkedList<>();
//
//		try {
//			controlinfos.addAll(cfresults.get());
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		} catch (ExecutionException e) {
//			e.printStackTrace();
//		}
//
////		System.out.println(controlinfos);
//
//		EntityManagerFactory emf =
//				Persistence.createEntityManagerFactory(
//						"/home/huoc/Projects/testassist/database/controlflow.odb");
//		EntityManager em = emf.createEntityManager();
//		em.getTransaction().begin();
//
//		for (ControlFlowInfo cfi : controlinfos) {
//			ControlFlowInfo old = em.find(ControlFlowInfo.class, cfi.identifier());
//			if (old == null) em.persist(cfi);
//			else 
//				old.setMap(cfi.getControlFlowMap());
//		}
//		em.getTransaction().commit();
//		em.flush();
//		em.close();
//		emf.close();
//
////		TypedQuery<ControlFlowInfo> query =
////				em.createQuery("SELECT p FROM ControlFlowInfo p", ControlFlowInfo.class);
////
////		List<ControlFlowInfo> res = query.getResultList();
////
//		emf =
//				Persistence.createEntityManagerFactory(
//						"/home/huoc/Projects/testassist/database/controlflow.odb");
//		em = emf.createEntityManager();
//		ControlFlowInfo currentCFInfo = em.find(ControlFlowInfo.class, "java.util.AbstractList.<init>()V");
//		System.out.println("find : " + currentCFInfo);
//
//		em.close();
//		emf.close();
	}
}

package edu.udel.testassist.analyzer.wala;

import java.util.Collection;
import java.util.HashSet;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.classLoader.ShrikeCTMethod;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.types.annotations.Annotation;
import com.ibm.wala.util.strings.Atom;

public class IMethods {

	public static Atom SETUP = Atom.findOrCreateAsciiAtom("setUp");
	public static Atom TEARDOWN = Atom.findOrCreateAsciiAtom("tearDown");

	private static class HasAnnotation implements Predicate<IMethod> {

		private final Predicate<Annotation> isAnnotation;

		public HasAnnotation(final TypeReference annotationReference) {

			this.isAnnotation = new Predicate<Annotation>() {
				public boolean apply(Annotation annotation) {
					return annotationReference.equals(annotation.getType());
				}
			};

		}

		public boolean apply(IMethod method) {

			if (!(method instanceof ShrikeCTMethod))
				return false;

			ShrikeCTMethod shrikeCTMethod = (ShrikeCTMethod) method;
			Collection<Annotation> annotations = new HashSet<>();

			try {
				annotations.addAll(shrikeCTMethod.getRuntimeVisibleAnnotations());
			}
			catch (InvalidClassFileException e) {
				e.printStackTrace();
			}

			return Iterables.any(annotations, isAnnotation);
		}
	}

	public static Predicate<IMethod> hasTestAnnotation = new HasAnnotation(Types.OrgJUnitTest);
	public static Predicate<IMethod> hasBeforeAnnotation = new HasAnnotation(Types.OrgJUnitBefore);
	public static Predicate<IMethod> hasAfterAnnotation = new HasAnnotation(Types.OrgJUnitAfter);

	// Predicate to check whether a method should be considered a test
	public static Predicate<IMethod> isTest = new Predicate<IMethod>() {
		public boolean apply(IMethod method) {

			// JUnit 4, test methods are annotated with @Test
			if (hasTestAnnotation.apply(method)) {
				return true;
			}

			// in JUnit 3, test methods are methods in subclasses of TestCase that start with "test"
			return method.getName().toString().startsWith("test")
					&& IClasses.isSubclassOfTestCase.apply(method.getDeclaringClass());
		}
	};

	public static Predicate<IMethod> isNamedSetUp = new Predicate<IMethod>() {
		public boolean apply(IMethod method) {
			return method.getName().equals(SETUP);
		}
	};

	public static Predicate<IMethod> isNamedTearDown = new Predicate<IMethod>() {
		public boolean apply(IMethod method) {
			return method.getName().equals(TEARDOWN);
		}
	};

	public static Predicate<IMethod> isBefore = new Predicate<IMethod>() {
		public boolean apply(IMethod method) {
			return hasBeforeAnnotation.apply(method);
		}
	};

	public static Predicate<IMethod> isAfter = new Predicate<IMethod>() {
		public boolean apply(IMethod method) {
			return hasAfterAnnotation.apply(method);
		}
	};

	public static Predicate<IMethod> isAbstract = new Predicate<IMethod>() {
		public boolean apply(IMethod method) {
			return method.isAbstract();
		}
	};

	public static Function<IMethod, String> SIGNATURE = new Function<IMethod, String>() {
		public String apply(final IMethod method) {
			return method.getSignature();
		}
	};

}

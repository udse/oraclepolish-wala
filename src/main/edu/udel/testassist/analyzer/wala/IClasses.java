package edu.udel.testassist.analyzer.wala;

import com.google.common.base.Predicate;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.cha.IClassHierarchy;

public class IClasses {

	public static Predicate<IClass> isApplication = new Predicate<IClass>() {
		public boolean apply(IClass cls) {
			AnalysisScope analysisScope = cls.getClassHierarchy().getScope();
			return analysisScope.getApplicationLoader().equals(cls.getClassLoader().getReference());
		}
	};

	public static Predicate<IClass> isPrimordial = new Predicate<IClass>() {
		public boolean apply(IClass cls) {
			AnalysisScope analysisScope = cls.getClassHierarchy().getScope();
			return analysisScope.getPrimordialLoader().equals(cls.getClassLoader().getReference());
		}
	};

	public static Predicate<IClass> isExtension = new Predicate<IClass>() {
		public boolean apply(IClass cls) {
			AnalysisScope analysisScope = cls.getClassHierarchy().getScope();
			return analysisScope.getExtensionLoader().equals(cls.getClassLoader().getReference());
		}
	};

	public static Predicate<IClass> isAbstract = new Predicate<IClass>() {
		public boolean apply(IClass cls) {
			return cls.isAbstract();
		}
	};

	public static Predicate<IClass> isInterface = new Predicate<IClass>() {
		public boolean apply(IClass cls) {
			return cls.isInterface();
		}
	};

	public static class IsSubclassOf implements Predicate<IClass> {

		private final IClass cls;
		private final IClassHierarchy classHierarchy;

		public IsSubclassOf(final IClass cls) {
			this.cls = cls;
			this.classHierarchy = cls.getClassHierarchy();
		}

		public boolean apply(final IClass other) {
			return classHierarchy.isSubclassOf(other, cls);
		}
	}

	public static Predicate<IClass> isSubclassOfTestCase = new Predicate<IClass>() {
		public boolean apply(final IClass cls) {
			IClassHierarchy classHierarchy = cls.getClassHierarchy();
			IClass testCaseClass = classHierarchy.lookupClass(Types.OrgJUnitTestCase);
			return !cls.equals(testCaseClass) && classHierarchy.isSubclassOf(cls, testCaseClass);
		}
	};

}

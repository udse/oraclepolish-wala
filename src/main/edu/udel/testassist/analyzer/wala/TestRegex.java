package edu.udel.testassist.analyzer.wala;

import java.util.regex.Pattern;

public class TestRegex {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String size = "java\\.util\\..*List\\.size";
		String next = "java\\.util\\..*List\\..*Itr.next";
		
		String w1 = "java.util.ArrayList.size";
		String w2 = "java.util.ArrayList$Itr.next";
		
		System.out.println("w1 : size " + w1.matches(size));
		System.out.println("w2 : next " + w2.matches(next));
	}

}

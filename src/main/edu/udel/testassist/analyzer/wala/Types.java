package edu.udel.testassist.analyzer.wala;

import com.ibm.wala.types.ClassLoaderReference;
import com.ibm.wala.types.TypeName;
import com.ibm.wala.types.TypeReference;

public class Types {

	private final static TypeName OrgJUnitTestName = TypeName.string2TypeName("Lorg/junit/Test");
	public final static TypeReference OrgJUnitTest = TypeReference.findOrCreate(ClassLoaderReference.Application,
			OrgJUnitTestName);

	private final static TypeName OrgJUnitBeforeName = TypeName.string2TypeName("Lorg/junit/Before");
	public final static TypeReference OrgJUnitBefore = TypeReference.findOrCreate(ClassLoaderReference.Application,
			OrgJUnitBeforeName);

	private final static TypeName OrgJUnitAfterName = TypeName.string2TypeName("Lorg/junit/After");
	public final static TypeReference OrgJUnitAfter = TypeReference.findOrCreate(ClassLoaderReference.Application,
			OrgJUnitAfterName);

	private final static TypeName OrgJUnitTestCaseName = TypeName.string2TypeName("Ljunit/framework/TestCase");
	public final static TypeReference OrgJUnitTestCase = TypeReference.findOrCreate(ClassLoaderReference.Extension,
			OrgJUnitTestCaseName);

	private final static TypeName JavaLangObjectName = TypeName.string2TypeName("Ljava/lang/Object");
	public final static TypeReference JavaLangObject = TypeReference.findOrCreate(ClassLoaderReference.Application,
			JavaLangObjectName);

	public final static TypeName StringName = TypeName.string2TypeName("Ljava/lang/String");
}

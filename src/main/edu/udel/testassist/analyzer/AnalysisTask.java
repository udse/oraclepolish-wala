package edu.udel.testassist.analyzer;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;

import com.google.common.collect.ImmutableSet;
import com.ibm.wala.classLoader.IBytecodeMethod;
import com.ibm.wala.ipa.callgraph.AnalysisCache;
import com.ibm.wala.ipa.cha.ClassHierarchy;
import com.ibm.wala.ssa.DefUse;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSACFG;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SymbolTable;

import edu.udel.testassist.configuration.TaintInfo;
import edu.udel.testassist.configuration.TestInfo;

public abstract class AnalysisTask implements Callable<TestInfo> {
	/*
	 * I. Patterns:
	 * 
	 * 1. 
	 * test(){ 
	 * 		actual = some_code 
	 * 		expected = some_code 
	 * 		assert(expected, actual) 
	 * 	}
	 * 
	 * 
	 * 2. 
	 * test(){ 
	 * 		try{ 
	 * 			v = some_code //this piece must succeed 
	 * 		} catch { 
	 * 			fail() 		//if a fail is in a catch block, find instructions may throw
	 * 						//corresponding exception in the pred blocks and put them in range 
	 * 		} 
	 * 	} 
	 * DataStructure
	 * 
	 * 
	 * 
	 * The following two patterns, focusing on exceptions. maybe we can consider 
	 * exceptions (brought by certain instructions) as inputs
	 * During the execution, if that exception occurs, that will have a score of, for example,
	 * 1/1.
	 * 
	 * 3. (annotated):
	 * 
	 * @Test(expected = some_exception) 
	 * 	test(){ 
	 * 		some_code 	//grab expected exceptions in the annotation, put candidates in 
	 * 					//the ranges[DONE] 
	 * 	} 
	 * jtopas, joda-converter Alltests.test_convert_annotationNoMethods
	 * 
	 * 4. 
	 * test(){ 
	 * 	try{ 
	 * 		v = some_code 	//this piece must throw exception
	 * 		fail() 			//if a fail is not in a catch block, ignore it. 
	 * 	} catch e {
	 *     sysout
	 * 		assert(e.getErrorMsg) 			//comparing error with some expected error 
	 * 										//no need to find the def of the actual in the assertion
	 * 										//instead, go to the pred block and find the 
	 * 										//instructions which may throw this kind of exception 
	 * 	} 
	 * } 
	 * Sudoku, joda-converter
	 * 
	 * 5. 
	 * test(){
	 * 	try{
	 * 		v = some_code
	 * 		fail()
	 * 	} catch e {
	 * 		//nothing: good
	 * 		//no code
	 *  }
	 * }
	 * 
	 * 5. customized assert: joda-converter test_Double
	 * 
	 */

	private static final AnalysisCache analysisCache = new AnalysisCache();

	protected final IBytecodeMethod method;
	protected final IR ir;
	protected final SymbolTable symbolTable;
	protected final DefUse du;
	protected final List<SSAInstruction> insns;
	protected Collection<TaintInfo> setups;
	protected final SSACFG cfg;
	protected final ClassHierarchy hierarchy;

	public AnalysisTask(final IBytecodeMethod method, final ClassHierarchy ch) throws IOException {
		this.method = method;
		this.ir = analysisCache.getIR(method);

		this.du = analysisCache.getDefUse(ir);
		this.symbolTable = ir.getSymbolTable();
		this.insns = Arrays.asList(ir.getInstructions());
		this.setups = null;
		this.cfg = ir.getControlFlowGraph();
		this.hierarchy = ch;
	}

	public AnalysisTask(final IBytecodeMethod method, Collection<TaintInfo> setups, final ClassHierarchy ch) throws IOException {
		this(method, ch);
		if (setups != null) this.setups = ImmutableSet.copyOf(setups);
	}
}
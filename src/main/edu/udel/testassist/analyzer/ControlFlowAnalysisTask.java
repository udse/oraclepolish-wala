package edu.udel.testassist.analyzer;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import com.ibm.wala.classLoader.IBytecodeMethod;
import com.ibm.wala.ipa.callgraph.AnalysisCache;
import com.ibm.wala.ipa.cha.ClassHierarchy;
import com.ibm.wala.ssa.DefUse;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSACFG;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SymbolTable;

import edu.udel.testassist.configuration.ControlFlowInfo;
import edu.udel.testassist.visitors.ControlFlowAnalyzer;

public class ControlFlowAnalysisTask implements Callable<ControlFlowInfo> {

	private static final AnalysisCache analysisCache = new AnalysisCache();

	protected final IBytecodeMethod method;
	protected final IR ir;
	protected final SymbolTable symbolTable;
	protected final DefUse du;
	protected final List<SSAInstruction> insns;
	protected final SSACFG cfg;
	protected final ClassHierarchy hierarchy;

	public ControlFlowAnalysisTask(final IBytecodeMethod method, final ClassHierarchy ch) {
		this.method = method;
		this.ir = analysisCache.getIR(method);

		this.du = analysisCache.getDefUse(ir);
		this.symbolTable = ir.getSymbolTable();
		this.insns = Arrays.asList(ir.getInstructions());
		this.cfg = ir.getControlFlowGraph();
		this.hierarchy = ch;
	}

	@Override
	public ControlFlowInfo call() throws Exception {
		ControlFlowAnalyzer branchAnalyzer = new ControlFlowAnalyzer(cfg, insns, method, du, symbolTable, hierarchy, ir);
		
		for (SSAInstruction ins : insns) {
			if (ins == null) continue;
			ins.visit(branchAnalyzer);
		}

		return new ControlFlowInfo(method.getSignature(), branchAnalyzer.getMap());
	}

}

package edu.udel.testassist.analyzer;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ScheduledThreadPoolExecutor;

import static com.google.common.base.Predicates.or;
import static com.google.common.base.Predicates.not;

import com.ibm.wala.classLoader.IClass;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.ibm.wala.classLoader.IBytecodeMethod;
import com.ibm.wala.ipa.cha.ClassHierarchy;
import com.ibm.wala.shrikeCT.InvalidClassFileException;

import edu.udel.testassist.analyzer.utils.Pair;
import edu.udel.testassist.analyzer.wala.IClasses;
import edu.udel.testassist.analyzer.wala.IMethods;
import edu.udel.testassist.configuration.TestInfo;
import edu.udel.testassist.configuration.TaintInfo;

public class Analyzer {

	final ListeningExecutorService executor = MoreExecutors
			.listeningDecorator(MoreExecutors.getExitingScheduledExecutorService(new ScheduledThreadPoolExecutor(1)));

	private IBytecodeMethod findSetup(IClass klass) {

		Iterable<IBytecodeMethod> setups =
				FluentIterable.from(klass.getDeclaredMethods())
						.filter(IBytecodeMethod.class)
						.filter(or(IMethods.isBefore, IMethods.isNamedSetUp));

		if (setups != null && setups.iterator().hasNext()) {
			return setups.iterator().next();
		}
		else {
			IClass sup = klass.getSuperclass();
			if (sup == null) return null;
			return findSetup(klass.getSuperclass());
		}

	}

	public ListenableFuture<List<TestInfo>> process(final ClassHierarchy classHierarchy) throws IOException {
		final String fn = "/home/huoc/Projects/testassist/testassist-jpf/walaresult.csv";

		Iterable<Pair<IBytecodeMethod, IBytecodeMethod>> allpairs = FluentIterable.from(classHierarchy)
				.filter(IClasses.isApplication)
//								.transform(new Function<IClass, IClass>() {
//									public IClass apply(IClass cls) {
//										System.out.println(cls);
//										return cls;
//									}
//								})
				.filter(not(or(IClasses.isAbstract, IClasses.isInterface)))
				.transformAndConcat(new Function<IClass, Iterable<Pair<IBytecodeMethod, IBytecodeMethod>>>() {
					public Iterable<Pair<IBytecodeMethod, IBytecodeMethod>> apply(IClass klass) {
						Iterable<IBytecodeMethod> methods =
								FluentIterable.from(klass.getDeclaredMethods())
										.filter(IBytecodeMethod.class);

						Iterable<IBytecodeMethod> tests = FluentIterable.from(methods)
								.filter(IMethods.isTest)
								.filter(not(IMethods.isAbstract));

						final IBytecodeMethod setup = findSetup(klass);

						return FluentIterable.from(tests)
								.transform(new Function<IBytecodeMethod, Pair<IBytecodeMethod, IBytecodeMethod>>() {
									public Pair<IBytecodeMethod, IBytecodeMethod> apply(IBytecodeMethod test) {
										return new Pair<IBytecodeMethod, IBytecodeMethod>(setup, test);
									}
								});
					}
				});

		Random rnd = new Random();
		rnd.setSeed(19716);

		List<Pair<IBytecodeMethod, IBytecodeMethod>> pairs = Lists.newArrayList(allpairs);
		Collections.shuffle(pairs, rnd);

		Iterable<ListenableFuture<TestInfo>> futures =
//				FluentIterable.from(Iterables.limit(pairs, 24))
				FluentIterable.from(pairs)
//						.filter(new Predicate<Pair<IBytecodeMethod, IBytecodeMethod>>() {
//							public boolean apply(Pair<IBytecodeMethod, IBytecodeMethod> pair) {
//								IBytecodeMethod test = pair.getRight();
//								return test.getName().toString().equals("testSplitByCharacterType");
//							}
//						})
						.transform(new Function<Pair<IBytecodeMethod, IBytecodeMethod>, ListenableFuture<TestInfo>>() {
							public ListenableFuture<TestInfo> apply(Pair<IBytecodeMethod, IBytecodeMethod> pair) {
								IBytecodeMethod setup = pair.getLeft();
								IBytecodeMethod test = pair.getRight();

								Collection<TaintInfo> setupInfo = null;

								if (setup != null) {
									try {
										setupInfo = new BasicAnalysisTask(setup, null, fn, classHierarchy).call().taints();
									} catch (IOException | InvalidClassFileException e) {
										e.printStackTrace();
									}
								}

								try {
									return executor.submit(new BasicAnalysisTask(test, setupInfo, fn, classHierarchy));
								} catch (IOException e) {
									e.printStackTrace();
								}

								return null;
							}
						});

		return Futures.allAsList(futures);
	}
}

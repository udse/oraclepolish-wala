package edu.udel.testassist.analyzer;

import com.ibm.wala.types.FieldReference;

public final class FieldEntry {
	final private FieldReference fref;
	final private Integer node;
	
	public FieldEntry(FieldReference fref, Integer node){
		this.fref = fref;
		this.node = node;
	}
	
	public FieldReference getRef(){
		return fref;
	}
	
	public Integer getNode(){
		return node;
	}
	
	@Override
	public boolean equals(Object that){
		if (that instanceof FieldEntry){
			FieldEntry fe = (FieldEntry) that;
//			return (this.fref.toString()+node.getNodeName())
//							.equals(fe.getRef().toString()+node.getNodeName());
			return this.toString().equals(fe.toString());
		}
		return false;
	}
//	
	@Override
	public int hashCode(){
		return this.toString().hashCode() % 200 + 200;
//		return 1;
	}
	
	@Override
	public String toString(){
//		return ""+this.hashCode();
		return this.fref.getSignature() + ":" 
		+ node.toString();
	}
}

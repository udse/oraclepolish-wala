package edu.udel.testassist.analyzer;

import java.io.IOException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;

import com.google.common.primitives.Ints;
import com.ibm.wala.classLoader.CallSiteReference;
import com.ibm.wala.classLoader.IBytecodeMethod;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.classLoader.ShrikeCTMethod;
import com.ibm.wala.ipa.callgraph.AnalysisCache;
import com.ibm.wala.ipa.cha.ClassHierarchy;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.ibm.wala.ssa.DefUse;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.ISSABasicBlock;
import com.ibm.wala.ssa.SSAArrayLoadInstruction;
import com.ibm.wala.ssa.SSAArrayStoreInstruction;
import com.ibm.wala.ssa.SSABinaryOpInstruction;
import com.ibm.wala.ssa.SSACFG;
import com.ibm.wala.ssa.SSACFG.ExceptionHandlerBasicBlock;
import com.ibm.wala.ssa.SSAConditionalBranchInstruction;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAInstruction.Visitor;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.ssa.SSALoadMetadataInstruction;
import com.ibm.wala.ssa.SSANewInstruction;
import com.ibm.wala.ssa.SSAPhiInstruction;
import com.ibm.wala.ssa.SSAPutInstruction;
import com.ibm.wala.ssa.SSAReturnInstruction;
import com.ibm.wala.ssa.SSAThrowInstruction;
import com.ibm.wala.ssa.SymbolTable;
import com.ibm.wala.types.ClassLoaderReference;
import com.ibm.wala.types.MethodReference;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.types.annotations.Annotation;
import com.ibm.wala.util.graph.dominators.Dominators;
import com.ibm.wala.util.graph.impl.InvertedGraph;
import com.ibm.wala.util.strings.StringStuff;
import com.ibm.wala.shrikeCT.AnnotationsReader.ElementValue;

import edu.udel.testassist.analyzer.wala.Types;
import edu.udel.testassist.configuration.Range;
import edu.udel.testassist.configuration.TaintInfo;
import edu.udel.testassist.configuration.TestInfo;

public abstract class CopyOfAnalysisTask implements Callable<TestInfo> {
	/*
	 * I. Patterns:
	 * 
	 * 1. 
	 * test(){ 
	 * 		actual = some_code 
	 * 		expected = some_code 
	 * 		assert(expected, actual) 
	 * 	}
	 * 
	 * 
	 * 2. 
	 * test(){ 
	 * 		try{ 
	 * 			v = some_code //this piece must succeed 
	 * 		} catch { 
	 * 			fail() 		//if a fail is in a catch block, find instructions may throw
	 * 						//corresponding exception in the pred blocks and put them in range 
	 * 		} 
	 * 	} 
	 * DataStructure
	 * 
	 * 
	 * 
	 * The following two patterns, focusing on exceptions. maybe we can consider 
	 * exceptions (brought by certain instructions) as inputs
	 * During the execution, if that exception occurs, that will have a score of, for example,
	 * 1/1.
	 * 
	 * 3. (annotated):
	 * 
	 * @Test(expected = some_exception) 
	 * 	test(){ 
	 * 		some_code 	//grab expected exceptions in the annotation, put candidates in 
	 * 					//the ranges[DONE] 
	 * 	} 
	 * jtopas, joda-converter Alltests.test_convert_annotationNoMethods
	 * 
	 * 4. 
	 * test(){ 
	 * 	try{ 
	 * 		v = some_code 	//this piece must throw exception
	 * 		fail() 			//if a fail is not in a catch block, ignore it. 
	 * 	} catch e {
	 *     sysout
	 * 		assert(e.getErrorMsg) 			//comparing error with some expected error 
	 * 										//no need to find the def of the actual in the assertion
	 * 										//instead, go to the pred block and find the 
	 * 										//instructions which may throw this kind of exception 
	 * 	} 
	 * } 
	 * Sudoku, joda-converter
	 * 
	 * 5. 
	 * test(){
	 * 	try{
	 * 		v = some_code
	 * 		fail()
	 * 	} catch e {
	 * 		//nothing: good
	 * 		//no code
	 *  }
	 * }
	 * 
	 * 5. customized assert: joda-converter test_Double
	 * 
	 * 
	 * II. Backtracking
	 * 
	 * Find the def of a possible actual value corresponding to each pattern
	 * 
	 * If a getdef() backtracks to a phi instruction, backtrack until we find a condition,
	 * start from the condition.
	 * 
	 * III. Result
	 * 
	 * 1. nothing from jpf run
	 * 
	 * 2. zero tainted fields/exceptions
	 * 
	 * 3. partial tainted fields/exceptions
	 * 
	 * 4. perfect
	 * 
	 * IV. Reason
	 * 
	 * A. structural (false positive)
	 * B. 
	 */

	private static final AnalysisCache analysisCache = new AnalysisCache();

	protected final IBytecodeMethod method;
	protected final IR ir;
	protected final SymbolTable symbolTable;
	protected final DefUse du;
	protected final List<SSAInstruction> insns;
	protected Collection<TaintInfo> setups;
	protected final SSACFG cfg;
	protected final ClassHierarchy hierarchy;
//	protected final Multimap<ISSABasicBlock, SSAInstruction> handlerMap;

	public CopyOfAnalysisTask(final IBytecodeMethod method, final ClassHierarchy ch) throws IOException {
		this.method = method;
		this.ir = analysisCache.getIR(method);

		this.du = analysisCache.getDefUse(ir);
		this.symbolTable = ir.getSymbolTable();
		this.insns = Arrays.asList(ir.getInstructions());
		this.setups = null;
		this.cfg = ir.getControlFlowGraph();
		this.hierarchy = ch;
//		this.handlerMap = getHandlerMap();
	}

	public CopyOfAnalysisTask(final IBytecodeMethod method, Collection<TaintInfo> setups, final ClassHierarchy ch) throws IOException {
		this(method, ch);
		if (setups != null) this.setups = ImmutableSet.copyOf(setups);
	}
//
//	public List<ISSABasicBlock> dominatingCatchBlocks(ISSABasicBlock bbl) {
//		return Lists.newArrayList(Iterables.filter(Lists.newArrayList(doms.dominators(bbl)),
//				new Predicate<ISSABasicBlock>() {
//					public boolean apply(ISSABasicBlock b) {
//						return b.isCatchBlock();
//					}
//				}));
//	}
//
//	Ordering<ISSABasicBlock> dominance = new Ordering<ISSABasicBlock>() {
//		public int compare(ISSABasicBlock left, ISSABasicBlock right) {
//			return doms.isDominatedBy(left, right) ? -1 : 1;
//		}
//	};
//
//	Ordering<ISSABasicBlock> postDominance = new Ordering<ISSABasicBlock>() {
//		public int compare(ISSABasicBlock left, ISSABasicBlock right) {
//			return pdoms.isDominatedBy(left, right) ? -1 : 1;
//		}
//	};
//
//	
//	protected Collection<Range> getRangesFromThrower(ISSABasicBlock bbl, final int pattern) {
//		if (!bbl.isCatchBlock()) return null;
//		final Set<TypeReference> caughtTypes =
//				Sets.newHashSet(((ExceptionHandlerBasicBlock) bbl).getCaughtExceptionTypes());
//		Collection<Range> ths = FluentIterable.from(handlerMap.get(bbl))
//				.filter(new Predicate<SSAInstruction>() {
//					public boolean apply(SSAInstruction i) {
//						Set<TypeReference> fromIns = getExceptionOfInstruction(i);
//						return mayThrowExceptionOf(fromIns, caughtTypes);
//					}
//				})
//				.transform(new Function<SSAInstruction, Range>() {
//					public Range apply(SSAInstruction i) {
//						Optional<Integer> line = lineNumberOf(i);
//						if (line.isPresent()) return new Range(line.get(), pattern);
//						return null;
//					}
//				})
//				.filter(Predicates.notNull())
//				.toList();
//
//		return ths;
//	}
//
//	@Deprecated
//	Function<ISSABasicBlock, Function<Set<ISSABasicBlock>, Set<ISSABasicBlock>>> union =
//			new Function<ISSABasicBlock, Function<Set<ISSABasicBlock>, Set<ISSABasicBlock>>>() {
//				public Function<Set<ISSABasicBlock>, Set<ISSABasicBlock>> apply(ISSABasicBlock bbl) {
//					Dominators<ISSABasicBlock> pds = Dominators.make(inverted, bbl);
//					final Set<ISSABasicBlock> pdoms = Sets.newHashSet(pds.dominators(bbl));
//					return new Function<Set<ISSABasicBlock>, Set<ISSABasicBlock>>() {
//						public Set<ISSABasicBlock> apply(Set<ISSABasicBlock> accu) {
//							return Sets.intersection(pdoms, accu);
//						}
//					};
//				}
//			};
//
//	@Deprecated
//	Function<ISSABasicBlock, Function<Set<ISSABasicBlock>, Set<ISSABasicBlock>>> intersection =
//			new Function<ISSABasicBlock, Function<Set<ISSABasicBlock>, Set<ISSABasicBlock>>>() {
//				public Function<Set<ISSABasicBlock>, Set<ISSABasicBlock>> apply(ISSABasicBlock bbl) {
//					Dominators<ISSABasicBlock> pds = Dominators.make(inverted, bbl);
//					final Set<ISSABasicBlock> pdoms = Sets.newHashSet(pds.dominators(bbl));
//					return new Function<Set<ISSABasicBlock>, Set<ISSABasicBlock>>() {
//						public Set<ISSABasicBlock> apply(Set<ISSABasicBlock> accu) {
//							return Sets.intersection(pdoms, accu);
//						}
//					};
//				}
//			};
//
//	protected Set<TypeReference> getExceptionOfInstruction(SSAInstruction ins) {
//		Set<TypeReference> types = new HashSet<TypeReference>();
//		if (ins instanceof SSAInvokeInstruction) {
//			SSAInvokeInstruction invoke = (SSAInvokeInstruction) ins;
//			MethodReference mref = invoke.getDeclaredTarget();
//			IMethod met = hierarchy.resolveMethod(mref);
//			IR localir = analysisCache.getIR(met);
//			if (localir != null) {
//				final DefUse localdu = analysisCache.getDefUse(localir);
//				List<SSAThrowInstruction> throwses =
//						FluentIterable.from(Lists.newArrayList(localir.getInstructions()))
//								.filter(new Predicate<SSAInstruction>() {
//									public boolean apply(SSAInstruction insn) {
//										return (insn instanceof SSAThrowInstruction);
//									}
//								})
//								.transform(new Function<SSAInstruction, SSAThrowInstruction>() {
//									public SSAThrowInstruction apply(SSAInstruction ins) {
//										return (SSAThrowInstruction) ins;
//									}
//								})
//								.toList();
//
//				types = FluentIterable.from(throwses)
//						.transform(new Function<SSAThrowInstruction, TypeReference>() {
//							public TypeReference apply(SSAThrowInstruction th) {
//								SSAInstruction def = localdu.getDef(th.getException());
//								if (def instanceof SSANewInstruction) {
//									SSANewInstruction _new = (SSANewInstruction) def;
//									return _new.getConcreteType();
//								}
//								return null;
//							}
//						})
//						.filter(Predicates.notNull())
//						.filter(new Predicate<TypeReference>() {
//							public boolean apply(TypeReference tr) {
//								return hierarchy.isSubclassOf(hierarchy.lookupClass(tr),
//										hierarchy.lookupClass(TypeReference.JavaLangException));
//							}
//						})
//						.toSet();
//			}
//		}
//
//		return Sets.union(types, Sets.newHashSet(ins.getExceptionTypes()));
//	}
//
//	@Deprecated
//	protected Set<TypeReference> getExceptionOfInstruction1(SSAInstruction ins) {
//		Set<TypeReference> mayThrow = new HashSet<TypeReference>();
//		if (ins instanceof SSAInvokeInstruction) {
//			SSAInvokeInstruction invoke = (SSAInvokeInstruction) ins;
//			MethodReference mref = invoke.getDeclaredTarget();
//			IMethod met = hierarchy.resolveMethod(mref);
//			try {
//				mayThrow = Sets.newHashSet(met.getDeclaredExceptions());
//				System.out.println(analysisCache.getIR(met));
//				System.out.println("declared : " + mref + mayThrow);
//			} catch (UnsupportedOperationException e) {
//				e.printStackTrace();
//			} catch (InvalidClassFileException e) {
//				e.printStackTrace();
//			}
//		}
//		return Sets.union(mayThrow, Sets.newHashSet(ins.getExceptionTypes()));
//	}
//
//	protected ImmutableSet<TypeReference> getAnnotatedException(IMethod m) {
//		if (!(m instanceof ShrikeCTMethod))
//			return null;
//
//		ShrikeCTMethod shrikeCTMethod = (ShrikeCTMethod) method;
//		Collection<Annotation> annotations;
//		try {
//			annotations = shrikeCTMethod.getRuntimeVisibleAnnotations();
//			Collection<Annotation> testAnnotations = FluentIterable.from(annotations)
//					.filter(new Predicate<Annotation>() {
//						public boolean apply(Annotation a) {
//							a.getNamedArguments().entrySet();
//							return a.getType().equals(Types.OrgJUnitTest);
//						}
//					})
//					.toList();
//
//			System.out.println("test anno : " + testAnnotations);
//
//			return FluentIterable.from(testAnnotations)
//					.transformAndConcat(new Function<Annotation, Collection<Entry<String, ElementValue>>>() {
//						public Collection<Entry<String, ElementValue>> apply(Annotation a) {
//							Map<String, ElementValue> map = a.getNamedArguments();
//							return map.entrySet();
//						}
//					})
//					.filter(new Predicate<Entry<String, ElementValue>>() {
//						public boolean apply(Entry<String, ElementValue> e) {
//							return e.getKey().contains("expected") && e.getValue().toString().contains("xception");
//						}
//					})
//					.transform(new Function<Entry<String, ElementValue>, TypeReference>() {
//						public TypeReference apply(Entry<String, ElementValue> e) {
//							TypeReference ta = TypeReference.findOrCreate(
//									ClassLoaderReference.Primordial, e.getValue().toString().replaceAll(";", ""));
//							return ta;
//						}
//					})
//					.toSet();
//
//		} catch (InvalidClassFileException e) {
//			e.printStackTrace();
//			return null;
//		}
//
//	}
//
//	protected boolean mayThrowExceptionOf(final Set<TypeReference> fromIns, final Set<TypeReference> annotated) {
//		return Iterables.any(fromIns,
//				new Predicate<TypeReference>() {
//					public boolean apply(TypeReference tr) {
//						final IClass klas = hierarchy.lookupClass(tr);
//						return Iterables.any(annotated,
//								new Predicate<TypeReference>() {
//									public boolean apply(TypeReference anno) {
//										IClass annoCls = hierarchy.lookupClass(anno);
//										if (annoCls == null || anno == null) return false;
//										return klas.equals(annoCls) || hierarchy.isSubclassOf(klas, annoCls);
//									}
//								});
//					}
//				});
//	}
//
//	protected Collection<Integer> relevantParametersOf(final SSAInstruction insn) {
//		if (insn instanceof SSAInvokeInstruction) {
//			SSAInvokeInstruction assertion = (SSAInvokeInstruction) insn;
//			return relevantParametersOfInvoke(assertion);
//		}
//		else if (insn instanceof SSAConditionalBranchInstruction)
//		{
//			SSAConditionalBranchInstruction cond = (SSAConditionalBranchInstruction) insn;
//			Collection<Integer> relevants = new ArrayList<Integer>();
//			for (int i = 0; i < cond.getNumberOfUses(); i++) {
//				if (!symbolTable.isConstant(cond.getUse(i))) {
//					relevants.add(insn.getUse(i));
//				}
//			}
//			return relevants;
//		}
//
//		return null;
//	}
//
//	protected Collection<Integer> relevantParametersOfInvoke(final SSAInvokeInstruction assertion) {
//
//		MethodReference declaredTarget = assertion.getDeclaredTarget();
//
//		String nameAndDescriptor = String.format("%s%s", declaredTarget.getName(), declaredTarget.getDescriptor());
//
//		switch (nameAndDescriptor) {
//
//		case "assertEquals(ZZ)V": // boolean
//		case "assertEquals(BB)V": // byte
//		case "assertEquals(CC)V": // char
//		case "assertEquals(DD)V": // double
//		case "assertEquals(DDD)V": // double
//		case "assertEquals(FF)V": // float
//		case "assertEquals(II)V": // int
//		case "assertEquals(JJ)V": // long
//		case "assertEquals(SS)V": // short
//		case "assertEquals(Ljava/lang/String;Ljava/lang/String;)V": // String
//		case "assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V": // Object
//
//		case "assertEquals([B[B)V": // byte
//		case "assertEquals([C[C)V": // char
//		case "assertEquals([I[I)V": // int
//		case "assertEquals([J[J)V": // long
//		case "assertEquals([S[S)V": // short
//		case "assertArrayEquals([Ljava/lang/Object;[Ljava/lang/Object;)V":
//
//		case "assertSame(Ljava/lang/Object;Ljava/lang/Object;)V":
//		case "assertNotSame(Ljava/lang/Object;Ljava/lang/Object;)V": {
//			final int expectedParameter = assertion.getUse(0);
//			final int actualParameter = assertion.getUse(1);
//
//			final boolean expectedParameterIsConstant = symbolTable.isConstant(expectedParameter);
//			final boolean actualParameterIsConstant = symbolTable.isConstant(actualParameter);
//
//			if (expectedParameterIsConstant && actualParameterIsConstant) {
//				System.out.printf("%s - expected and actual parameters are both constants\n", locationOf(assertion));
//				return ImmutableList.of(actualParameter);
//			}
//			else if (!expectedParameterIsConstant && actualParameterIsConstant) {
//				System.out.printf(
//						"%s - expected parameter is not a constant while actual parameter is a constant, assuming inverse usage\n",
//						locationOf(assertion));
//				return ImmutableList.of(expectedParameter);
//			}
//			else if (expectedParameterIsConstant && !actualParameterIsConstant) {
//				// System.out.printf("\t%s@%s - expected parameter is a constant while actual parameter is not a constant\n",
//				// declaredTarget.getName(), locationOf(assertion));
//				return ImmutableList.of(actualParameter);
//			}
//			else {
//				// System.out.printf("\t%s@%s - expected and actual parameters are both not constants, assuming correct usage\n",
//				// declaredTarget.getName(), locationOf(assertion));
//				return ImmutableList.of(actualParameter);
//			}
//		}
//
//		case "assertEquals(Ljava/lang/String;ZZ)V": // boolean
//		case "assertEquals(Ljava/lang/String;BB)V": // byte
//		case "assertEquals(Ljava/lang/String;CC)V": // char
//		case "assertEquals(Ljava/lang/String;DD)V": // double
//		case "assertEquals(Ljava/lang/String;DDD)V": // double
//		case "assertEquals(Ljava/lang/String;FF)V": // float
//		case "assertEquals(Ljava/lang/String;II)V": // int
//		case "assertEquals(Ljava/lang/String;JJ)V": // long
//		case "assertEquals(Ljava/lang/String;SS)V": // short
//		case "assertEquals(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V": // String
//		case "assertEquals(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V": // Object
//
//		case "assertEquals(Ljava/lang/String;[B[B)V": // byte
//		case "assertEquals(Ljava/lang/String;[C[C)V": // char
//		case "assertEquals(Ljava/lang/String;[I[I)V": // int
//		case "assertEquals(Ljava/lang/String;[J[J)V": // long
//		case "assertEquals(Ljava/lang/String;[S[S)V": // short
//		case "assertArrayEquals(Ljava/lang/String;[Ljava/lang/Object;[Ljava/lang/Object;)V":
//
//		case "assertSame(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V": // Object
//		case "assertNotSame(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V": // Object
//		{
//			final int expectedParameter = assertion.getUse(1);
//			final int actualParameter = assertion.getUse(2);
//
//			final boolean expectedParameterIsConstant = symbolTable.isConstant(expectedParameter);
//			final boolean actualParameterIsConstant = symbolTable.isConstant(actualParameter);
//
//			if (expectedParameterIsConstant && actualParameterIsConstant) {
//				System.out.printf("%s - expected and actual parameters are both constants\n", locationOf(assertion));
//				return ImmutableList.of(actualParameter);
//			}
//			else if (!expectedParameterIsConstant && actualParameterIsConstant) {
//				System.out.printf(
//						"%s - expected parameter is not a constant while actual parameter is a constant, assuming inverse usage\n",
//						locationOf(assertion));
//				return ImmutableList.of(expectedParameter);
//			}
//			else if (expectedParameterIsConstant && !actualParameterIsConstant) {
//				// System.out.printf("\t%s@%s - expected parameter is a constant while actual parameter is not a constant\n",
//				// declaredTarget.getName(), locationOf(assertion));
//				return ImmutableList.of(actualParameter);
//			}
//			else {
//				// System.out.printf("\t%s@%s - expected and actual parameters are both not constants, assuming correct usage\n",
//				// declaredTarget.getName(), locationOf(assertion));
//				return ImmutableList.of(actualParameter);
//			}
//		}
//		case "assertTrue(Z)V":
//		case "assertFalse(Z)V":
//		case "assertNull(Ljava/lang/Object;)V":
//		case "assertNotNull(Ljava/lang/Object;)V": {
//
//			final int actualParameter = assertion.getUse(0);
//
//			final boolean actualParameterIsConstant = symbolTable.isConstant(actualParameter);
//
//			if (actualParameterIsConstant) {
//				System.out.printf("%s - actual parameter is a constant\n", locationOf(assertion));
//			}
//
//			return ImmutableList.of(actualParameter);
//		}
//		case "assertTrue(Ljava/lang/String;Z)V":
//		case "assertFalse(Ljava/lang/String;Z)V":
//		case "assertNull(Ljava/lang/String;Ljava/lang/Object;)V":
//		case "assertNotNull(Ljava/lang/String;Ljava/lang/Object;)V": {
//
//			final int actualParameter = assertion.getUse(1);
//
//			final boolean actualParameterIsConstant = symbolTable.isConstant(actualParameter);
//
//			if (actualParameterIsConstant) {
//				System.out.printf("%s - actual parameter is a constant\n", locationOf(assertion));
//			}
//
//			return ImmutableList.of(actualParameter);
//		}
//		default: {
//			System.out.printf("%s - custom assertion (%s)\n", locationOf(assertion), declaredTarget.getName());
//
//			int firstParameterIndex = assertion.isStatic() ? 0 : 1;
//			int numberOfUses = assertion.isStatic() ? assertion.getNumberOfUses() : assertion.getNumberOfUses() - 1;
//
//			int[] relevantParameters = new int[numberOfUses];
//
//			for (int i = 0; i < numberOfUses; i++) {
//				relevantParameters[i] = assertion.getUse(firstParameterIndex + i);
//			}
//			return ImmutableList.copyOf(Ints.asList(relevantParameters));
//		}
//		}
//	}
//
//	protected abstract boolean validInput(int use, int param);
//
//	protected boolean loadingConstant(int use) {
//		SSAInstruction def = du.getDef(use);
//		if (null == def) return false;
//		if (def instanceof SSALoadMetadataInstruction) return true;
//		return false;
//	}
//
//	protected Collection<TaintInfo> genTaintInfo(Set<SSAInstruction> pinputs, int param) {
//		Collection<TaintInfo> taints = new HashSet<TaintInfo>();
//		for (SSAInstruction input : pinputs) {
//			for (int i = 0; i < input.getNumberOfUses(); i++) {
//				int use = input.getUse(i);
//				if (symbolTable.isConstant(use) || loadingConstant(use)) {
//					if (i == 1 && input instanceof SSAArrayStoreInstruction)
//						continue;
//					int pos = param == -1 ? i : param;
//					if (validInput(use, pos)) {
//						TaintInfo ti = taintInsn(input, use);
//						if (ti != null) taints.add(ti);
//					}
//				}
//			}
//		}
//
//		return taints;
//	}
//
//	protected Optional<Integer> lineNumberOf(final SSAInstruction insn) {
//		Optional<Integer> bco = bytecodeIndexOf(insn);
//		if (bco.isPresent()) {
//			return Optional.fromNullable(method.getLineNumber(bco.get()));
//		}
//		else {
//			return Optional.absent();
//		}
//	}
//
//	protected Optional<Integer> bytecodeIndexOf(final SSAInstruction insn) {
//
//		int insnIndex = Iterables.indexOf(insns, Predicates.equalTo(insn));
//
//		if (-1 != insnIndex) {
//			try {
//				int bytecodeIndex = method.getBytecodeIndex(insnIndex);
//				return Optional.of(bytecodeIndex);
//			} catch (InvalidClassFileException e) {
//				e.printStackTrace();
//			}
//		}
//
//		return Optional.absent();
//	}
//
//	protected String locationOf(final SSAInstruction insn) {
//		return String.format("%s:%d", StringStuff.jvmToReadableType
//				(method.getDeclaringClass().getName().toString()), bytecodeIndexOf(insn).or(-1));
//	}
//
//	protected int getTypeSize(TypeReference tref) {
//		return (tref.equals(TypeReference.Double) || tref.equals(TypeReference.Long)) ? 2 : 1;
//	}
//
//	protected Multimap<ISSABasicBlock, SSAInstruction> getHandlerMap() {
//		Multimap<ISSABasicBlock, SSAInstruction> handlerMap = HashMultimap.create();
//		Iterator<ISSABasicBlock> blockIter = cfg.iterator();
//		while (blockIter.hasNext()) {
//			ISSABasicBlock block = blockIter.next();
//			Iterator<SSAInstruction> insnIter = block.iterator();
//
//			while (insnIter.hasNext()) {
//				SSAInstruction insn = insnIter.next();
//
//				for (ISSABasicBlock successor : cfg.getExceptionalSuccessors(block)) {
//					if (!successor.isCatchBlock()) continue;
//					ExceptionHandlerBasicBlock handler = (ExceptionHandlerBasicBlock) successor;
//					handlerMap.put(successor, insn);
//				}
//			}
//		}
//		System.out.println("handlerMap : " + handlerMap);
//
//		return handlerMap;
//	}
//
//	protected Range getRange(SSAInstruction ins) {
//		Optional<Integer> line = lineNumberOf(ins);
//		if (line.isPresent()) return new Range(line.get(), 1);
//		return null;
//	}
//
//	protected Collection<Range> rangeFromInsWithExcep(Collection<SSAInstruction> insns, final Set<TypeReference> trs) {
//		return FluentIterable.from(insns)
//				.filter(new Predicate<SSAInstruction>() {
//					public boolean apply(SSAInstruction insn) {
//						Set<TypeReference> fromInsn = new HashSet<TypeReference>(insn.getExceptionTypes());
//						return mayThrowExceptionOf(fromInsn, trs) && lineNumberOf(insn).isPresent();
//					}
//				})
//				.transform(new Function<SSAInstruction, Range>() {
//					public Range apply(SSAInstruction insn) {
//						return new Range(lineNumberOf(insn).get(), 1);
//					}
//				}).toList();
//	}
//
//	protected Collection<Range> findThrower(ISSABasicBlock block) {
//		ExceptionHandlerBasicBlock handler = (ExceptionHandlerBasicBlock) block;
//		final Set<TypeReference> trs = new HashSet<TypeReference>(Lists.newArrayList(handler.getCaughtExceptionTypes()));
//		Collection<Range> temp = new ArrayList<Range>();
//
//		for (ISSABasicBlock bbl : cfg.getExceptionalPredecessors(block)) {
//			List<SSAInstruction> insns = Lists.newArrayList(bbl.iterator());
//			temp.addAll(rangeFromInsWithExcep(insns, trs));
//		}
//
//		return temp;
//	}
//
//	protected ISSABasicBlock findCatcher(ISSABasicBlock block, final int depth) {
//		if (block.isCatchBlock())
//			return block;
//		else {
//			Collection<ISSABasicBlock> preds = cfg.getNormalPredecessors(block);
//			if (preds.isEmpty() || depth == 0) return null;
//			return Iterables.find(FluentIterable.from(preds)
//					.transform(new Function<ISSABasicBlock, ISSABasicBlock>() {
//						public ISSABasicBlock apply(ISSABasicBlock bbl) {
//							return findCatcher(bbl, depth - 1);
//						}
//					}), Predicates.notNull(), null);
//		}
//	}
//
//	@Deprecated
//	protected Collection<Range> findPeer1(ISSABasicBlock block,
//			Multimap<SSAInstruction, SSAInstruction> handlerMap) {
//		Collection<Range> temp = new ArrayList<Range>();
//
//		ExceptionHandlerBasicBlock handler = (ExceptionHandlerBasicBlock) block;
//		SSAInstruction catchInsn = handler.getCatchInstruction();
//		System.out.println("catch ins " + catchInsn);
//
//		for (SSAInstruction peer : handlerMap.get(catchInsn)) {
//			System.out.println("peer : " + peer);
//			Optional<Integer> line = lineNumberOf(peer);
//			if (line.isPresent()) temp.add(new Range(line.get(), 1));
//		}
//		return temp;
//	}
//
//	protected SSAConditionalBranchInstruction findDomCond(List<ISSABasicBlock> space, final Set<ISSABasicBlock> workingset) {
//		// depth first search
//		if (space.size() == 0) return null;
//		ISSABasicBlock block = space.remove(space.size() - 1);
//		SSAConditionalBranchInstruction cond = findCondInBlock(block);
//		if (cond != null)
//			return cond;
//		else {
//			List<ISSABasicBlock> preds = FluentIterable.from(cfg.getNormalPredecessors(block))
//							.filter(new Predicate<ISSABasicBlock>(){
//								public boolean apply(ISSABasicBlock b){
//									return workingset.contains(b);
//								}
//							}
//					).toList();
//			space.addAll(preds);
//			workingset.removeAll(preds);
//			return findDomCond(space, workingset);
//		}
//	}
//
//	private SSAConditionalBranchInstruction findCondInBlock(ISSABasicBlock block) {
//		List<SSAInstruction> insns = Lists.newArrayList(block.iterator());
//		for (SSAInstruction ins : Lists.reverse(insns)) {
//			if (ins instanceof SSAConditionalBranchInstruction)
//				return (SSAConditionalBranchInstruction) ins;
//		}
//
//		return null;
//	}
//
//	protected Collection<Range> getDefRange(SSAInstruction ins, final int pattern) {
//		Collection<Range> temp = new ArrayList<Range>();
//		for (int parameter : relevantParametersOf(ins)) {
//			SSAInstruction def = du.getDef(parameter);
//			if (null == def)
//				continue;
//			else if (def instanceof SSAPhiInstruction) {
//				ISSABasicBlock block = cfg.getBlockForInstruction(insns.indexOf(ins));
//				SSAConditionalBranchInstruction cond =
//						findDomCond(Lists.newArrayList(new ISSABasicBlock[] { block }), 
//																	Sets.newHashSet(cfg));
//				if (null == cond || cond.equals(ins)) continue;
//				return getDefRange(cond, pattern);
//			}
//			else {
//				Optional<Integer> line = lineNumberOf(def);
//				if (line.isPresent()) temp.add(new Range(line.get(), pattern));
//			}
//		}
//		return temp;
//	}
//
//	protected TaintInfo taintInsn(SSAInstruction ins, int v) {
//		Collection<Integer> offsets = new HashSet<Integer>();
//
//		if (ins instanceof SSAInvokeInstruction) {
//			SSAInvokeInstruction invoke = (SSAInvokeInstruction) ins;
//			CallSiteReference csr = invoke.getCallSite();
//
//			MethodReference mref = csr.getDeclaredTarget();
//
//			int offset = 0;
//			for (int i = mref.getNumberOfParameters() - 1; i >= 0; i--) {
//				TypeReference tref = mref.getParameterType(i);
//				int size = getTypeSize(tref);
//				int use = invoke.getUse(i);
//
//				if (!csr.isStatic()) use = invoke.getUse(i + 1);
//				if (symbolTable.isConstant(use) || loadingConstant(use)) {
//					if (use == v) offsets.add(offset + ((size == 2) ? 1 : 0));
//				}
//
//				offset += size;
//			}
//		}
//		else if (ins instanceof SSAPutInstruction) {
//			SSAPutInstruction put = (SSAPutInstruction) ins;
//
//			int use = put.getVal();
//
//			if (symbolTable.isConstant(use)) {
//				TypeReference tref = put.getDeclaredFieldType();
//				if (use == v) offsets.add((getTypeSize(tref) == 2) ? 1 : 0);
//			}
//		}
//		else if (ins instanceof SSABinaryOpInstruction
//		// || ins instanceof SSANewInstruction
//		) {
//			for (int i = ins.getNumberOfUses() - 1; i >= 0; i--) {
//				int use = ins.getUse(i);
//				if (symbolTable.isConstant(use) || symbolTable.isLongConstant(use)) {
//					if (use == v) offsets.add(symbolTable.isConstant(use) ? 1 : 0);
//				}
//			}
//		}
//		else if (ins instanceof SSAArrayLoadInstruction) {
//			return null;
//		}
//		else if (ins instanceof SSAArrayStoreInstruction) {
//			SSAArrayStoreInstruction store = (SSAArrayStoreInstruction) ins;
//			int use = store.getUse(2);
////			if (use == v) offsets.add(symbolTable.isConstant(use) ? 1 : 0);
//			if (use == v) offsets.add(symbolTable.isDoubleConstant(use) || 
//								symbolTable.isLongConstant(use) ? 1 : 0);
//			System.out.println(store.getDef() + store.getUse(0) + store.getUse(1));
//		}
//		else if (ins instanceof SSAReturnInstruction) {
//			return null;
//		}
//		else if (ins.getNumberOfUses() == 0) {
//			return null;
//		}
//		else {
//			System.out.println("not caught " + ins);
//			return null;
//		}
//
//		Integer bco = bytecodeIndexOf(ins).get();
//		if (null == bco) return null;
//
//		return new TaintInfo(bco, offsets);
//	}
//
//	public abstract TestInfo call() throws IOException, InvalidClassFileException;
}
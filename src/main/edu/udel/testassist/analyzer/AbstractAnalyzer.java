package edu.udel.testassist.analyzer;

import java.util.List;

import com.google.common.base.Optional;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.ibm.wala.classLoader.CallSiteReference;
import com.ibm.wala.classLoader.IBytecodeMethod;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.ipa.cha.ClassHierarchy;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.ibm.wala.ssa.DefUse;
import com.ibm.wala.ssa.SSACFG;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAInstruction.Visitor;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.ssa.SSALoadMetadataInstruction;
import com.ibm.wala.types.TypeReference;

public abstract class AbstractAnalyzer extends Visitor {

	protected List<SSAInstruction> insns;
	protected SSACFG cfg;
	protected IBytecodeMethod method;
	protected DefUse du;

	public AbstractAnalyzer(final SSACFG cfg,
			final List<SSAInstruction> insns,
			final IBytecodeMethod method,
			final DefUse du) {
		this.cfg = cfg;
		this.insns = insns;
		this.method = method;
		this.du = du;
	}

	protected int getTypeSize(TypeReference tref) {
		return (tref.equals(TypeReference.Double) || tref.equals(TypeReference.Long)) ? 2 : 1;
	}

	protected boolean loadingMethod(int use, TypeReference tklass, ClassHierarchy cha) {
		SSAInstruction def = du.getDef(use);
		if (null == def) return false;
		if (def instanceof SSAInvokeInstruction) {
			SSAInvokeInstruction invoke = (SSAInvokeInstruction) def;

			IClass mklass = cha.lookupClass(invoke.getDeclaredTarget().getDeclaringClass());
			IClass testkass = cha.lookupClass(tklass);
			try {
				if (!(cha.isSubclassOf(testkass, mklass)
				|| testkass.equals(mklass)))
					return false;
			} catch (NullPointerException e) {
				return false;
			}

			CallSiteReference csr = invoke.getCallSite();

			if (!invoke.hasDef()) return false;

			if (csr.isStatic())
				return invoke.getNumberOfParameters() == 0;
			else
				return invoke.getNumberOfParameters() == 1;
		}
		return false;
	}

	protected boolean loadingConstant(int use) {
		SSAInstruction def = du.getDef(use);
		if (null == def) return false;
		if (def instanceof SSALoadMetadataInstruction) return true;
		return false;
	}

	protected Optional<Integer> bytecodeIndexOf(final SSAInstruction insn) {

		int insnIndex = Iterables.indexOf(insns, Predicates.equalTo(insn));

		if (-1 != insnIndex) {
			try {
				int bytecodeIndex = method.getBytecodeIndex(insnIndex);
				return Optional.of(bytecodeIndex);
			} catch (InvalidClassFileException e) {
				e.printStackTrace();
			}
		}

		return Optional.absent();
	}
}

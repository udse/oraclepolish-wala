package edu.udel.testassist.analyzer;

import static com.google.common.base.Predicates.not;
import static com.google.common.base.Predicates.or;

import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.ibm.wala.classLoader.IBytecodeMethod;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.ipa.cha.ClassHierarchy;

import edu.udel.testassist.analyzer.wala.IClasses;
import edu.udel.testassist.analyzer.wala.IMethods;
import edu.udel.testassist.configuration.ControlFlowInfo;

public class CFAnalyzer {
	final ListeningExecutorService executor = MoreExecutors
			.listeningDecorator(MoreExecutors.
					getExitingScheduledExecutorService(new ScheduledThreadPoolExecutor(1)));

	public ListenableFuture<List<ControlFlowInfo>> process(final ClassHierarchy classHierarchy) {
		Iterable<ListenableFuture<ControlFlowInfo>> futures = FluentIterable.from(classHierarchy)
								.filter(or(IClasses.isApplication, IClasses.isExtension))
//				.filter(IClasses.isPrimordial)
//												.transform(new Function<IClass, IClass>() {
//													public IClass apply(IClass cls) {
//														if (cls.toString().contains("EqualsBuilder")) {
//															System.out.println(cls);
//														}
//														return cls;
//													}
//												})
//				.filter(new Predicate<IClass>() {
//					public boolean apply(IClass cls) {
//						if (cls.toString().contains("EqualsBuilder")) {
//							return true;
//						}
//						return false;
//					}
//				})
				.filter(not(or(IClasses.isAbstract, IClasses.isInterface)))
				//				.filter(not(IClasses.isInterface))
				.transformAndConcat(new Function<IClass, Iterable<IBytecodeMethod>>() {
					public Iterable<IBytecodeMethod> apply(IClass klass) {
						Iterable<IBytecodeMethod> methods =
								FluentIterable.from(klass.getDeclaredMethods())
										.filter(IBytecodeMethod.class);
//						System.exit(0);
						return methods;
					}
				})
				.filter(new Predicate<IBytecodeMethod>() {
					public boolean apply(IBytecodeMethod method) {
						return !method.isNative();
					}
				})
				.filter(not(IMethods.isTest))
				.transform(new Function<IBytecodeMethod, ListenableFuture<ControlFlowInfo>>() {
					public ListenableFuture<ControlFlowInfo> apply(IBytecodeMethod method) {
						return executor.submit(new ControlFlowAnalysisTask(method, classHierarchy));
					}
				});

		return Futures.allAsList(futures);
	}
}

package edu.udel.testassist.analyzer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;

import com.ibm.wala.classLoader.IBytecodeMethod;
import com.ibm.wala.ipa.cha.ClassHierarchy;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.ibm.wala.ssa.SSAInstruction;

import edu.udel.testassist.configuration.TaintInfo;
import edu.udel.testassist.configuration.TestInfo;
import edu.udel.testassist.visitors.BasicInputAnalyzer;
import edu.udel.testassist.visitors.DataFlowAnalyzer;

public class BasicAnalysisTask extends AnalysisTask {
	private PrintWriter pw;

	public BasicAnalysisTask(final IBytecodeMethod method,
								final Collection<TaintInfo> setups, 
								final String fn, 
								final ClassHierarchy ch) throws IOException {

		super(method, setups, ch);
		this.pw = new PrintWriter(new BufferedWriter(new FileWriter(fn, true)));
	}

	protected boolean validInput(int use, int param) {
		return true;
	}

	@Override
	public TestInfo call() throws IOException, InvalidClassFileException {
		
		BasicInputAnalyzer basicInputAnalyzer = new BasicInputAnalyzer(cfg, insns, ir, method, du, hierarchy);
		DataFlowAnalyzer dataFlowAnalyzer = new DataFlowAnalyzer(cfg, insns, method, du, symbolTable);
		
		System.out.println(method);
		System.out.println(ir);
		
		for (SSAInstruction ins : insns) {
			if (ins == null) continue;
			ins.visit(basicInputAnalyzer);
			ins.visit(dataFlowAnalyzer);
		}
		
		int inputs = basicInputAnalyzer.totalInputs();

		pw.printf("%s, %s, %s, %s\n", method.getSignature(), inputs, inputs, 1);
		
		pw.close();
		
		return new TestInfo(method.getSignature(), 
									dataFlowAnalyzer.getOracleRanges(), 
									setups, 
									basicInputAnalyzer.getTaintInfos());
	}

}

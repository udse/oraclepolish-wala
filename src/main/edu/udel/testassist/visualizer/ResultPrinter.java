package edu.udel.testassist.visualizer;

import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.filter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Map;

import com.google.common.collect.Multimap;
import com.google.common.util.concurrent.FutureCallback;
import com.sun.jdi.ClassObjectReference;
import com.sun.jdi.Field;
import com.sun.jdi.ObjectReference;
import com.sun.jdi.ReferenceType;

import edu.udel.testassist.visualizer.jdi.Fields;

public class ResultPrinter implements FutureCallback<TestResult> {

	private final File outputDirectory;

	public ResultPrinter(final File outputDirectory) {
		this.outputDirectory = outputDirectory;
	}

	@Override
	public void onFailure(Throwable throwable) {
		throwable.printStackTrace();
	}

	@Override
	public void onSuccess(TestResult result) {

		String fileName = String.format("%s.%s.dot", result.className(), result.methodName());

		try (PrintWriter writer = new PrintWriter(new File(outputDirectory, fileName))) {

			Multimap<ReferenceType, ObjectReference> objects = result.objects();
			Multimap<ObjectReference, Field> modifications = result.modifications();
			Multimap<ObjectReference, Field> accesses = result.accesses();

			for (Map.Entry<ReferenceType, Collection<ObjectReference>> entry : objects.asMap().entrySet()) {

				ReferenceType referenceType = entry.getKey();
				ClassObjectReference classObject = referenceType.classObject();
				
				writer.printf("%s\n", referenceType.name());

				for (Field field : filter(referenceType.visibleFields(), Fields.IS_STATIC)) {
					writer.printf("\t\t%s - accessed %b, modified %b\n", field.name(),
							accesses.containsEntry(classObject, field),
							modifications.containsEntry(classObject, field));
				}

				Collection<ObjectReference> instances = entry.getValue();
				
				for (ObjectReference instance : instances) {
					
					writer.printf("\t%s[%d]\n", referenceType.name(), instance.uniqueID());
										
					for (Field field : filter(referenceType.visibleFields(), not(Fields.IS_STATIC))) {
						writer.printf("\t\t%s - accessed %b, modified %b\n", field.name(),
								accesses.containsEntry(instance, field),
								modifications.containsEntry(instance, field));
					}
				}
				
				writer.println();
			}
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}

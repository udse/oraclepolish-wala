package edu.udel.testassist.visualizer;

import com.google.common.collect.Multimap;
import com.sun.jdi.Field;
import com.sun.jdi.ObjectReference;
import com.sun.jdi.ReferenceType;

import edu.udel.testassist.configuration.MethodIdentifier;

public class TestResult {

	private final MethodIdentifier identifier;
	private Multimap<ReferenceType, ObjectReference> testObjectsByType;
	private Multimap<ObjectReference, Field> modifications;
	private Multimap<ObjectReference, Field> accesses;

	public TestResult(final MethodIdentifier methodIdentifier, final Multimap<ReferenceType, ObjectReference> testObjectsByType,
			Multimap<ObjectReference, Field> modifications, Multimap<ObjectReference, Field> accesses) {

		this.identifier = methodIdentifier;
		this.testObjectsByType = testObjectsByType;
		this.modifications = modifications;
		this.accesses = accesses;
	}

	public String className() {
		return identifier.className();
	}

	public String methodName() {
		return identifier.methodName();
	}

	public String methodSignature() {
		return identifier.methodSignature();
	}
	
	public Multimap<ReferenceType, ObjectReference> objects() {
		return testObjectsByType;
	}

	public Multimap<ObjectReference, Field> modifications() {
		return modifications;
	}

	public Multimap<ObjectReference, Field> accesses() {
		return accesses;
	}

}

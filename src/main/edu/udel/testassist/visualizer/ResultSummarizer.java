package edu.udel.testassist.visualizer;

import static com.google.common.base.Predicates.not;

import java.io.PrintWriter;
import java.io.Writer;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.FluentIterable;
import com.google.common.collect.Multimap;
import com.google.common.util.concurrent.FutureCallback;
import com.sun.jdi.ClassObjectReference;
import com.sun.jdi.Field;
import com.sun.jdi.ObjectReference;
import com.sun.jdi.ReferenceType;

import edu.udel.testassist.visualizer.jdi.Fields;

public class ResultSummarizer implements FutureCallback<TestResult>, AutoCloseable {

	private final PrintWriter output;

	public ResultSummarizer(final Writer writer) {
		output = new PrintWriter(writer);
		output.printf("\"test\", \"# objects\", \"# fields\", \"# modified\", \"# accessed\"\n");
	}

	@Override
	public void onFailure(Throwable throwable) {
		throwable.printStackTrace();
	}

	@Override
	public void onSuccess(TestResult testResult) {

		String className = testResult.className();
		String methodName = testResult.methodName();
		String methodSignature = testResult.methodSignature();

		Multimap<ReferenceType, ObjectReference> objects = testResult.objects();
		Multimap<ObjectReference, Field> modifications = testResult.modifications();
		Multimap<ObjectReference, Field> accesses = testResult.accesses();
      		
		int numFields = 0;
		for (Map.Entry<ReferenceType, Collection<ObjectReference>> entry : objects.asMap().entrySet()) {

			ReferenceType referenceType = entry.getKey();
			Collection<ObjectReference> instances = entry.getValue();
			
			int numStaticFields = FluentIterable.from(referenceType.visibleFields()).filter(Fields.IS_STATIC).size();

			int numInstanceFields = FluentIterable.from(referenceType.visibleFields()).filter(not(Fields.IS_STATIC)).size(); 
			for(Field field:referenceType.visibleFields())
			{
				if(field.name().equals("this$0")) numInstanceFields--;
			}
			numFields += numStaticFields + (numInstanceFields * instances.size());
						
		}

		output.printf("%s.%s%s, %d, %d, %d, %d\n", className, methodName, methodSignature, objects.size(), numFields, modifications.size(),
				accesses.size());
		output.flush();
		
				
	}

	@Override
	public void close() throws Exception {
		output.close();
	}
}

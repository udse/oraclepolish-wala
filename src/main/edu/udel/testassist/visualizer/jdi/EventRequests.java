package edu.udel.testassist.visualizer.jdi;

import java.util.LinkedList;
import java.util.List;

import com.google.common.collect.ImmutableList;
import com.sun.jdi.request.EventRequest;

public class EventRequests {

	private static final String HANDLER = EventRequests.class.getName() + "HANDLER";

	public static void appendHandler(EventRequest request, EventHandler<?> handler) {

		if (null == request.getProperty(HANDLER)) {
			request.putProperty(HANDLER, new LinkedList<>());
		}

		@SuppressWarnings("unchecked")
		LinkedList<EventHandler<?>> handlers = (LinkedList<EventHandler<?>>) request.getProperty(HANDLER);

		handlers.addLast(handler);
	}

	public static void prependHandler(EventRequest request, EventHandler<?> handler) {

		if (null == request.getProperty(HANDLER)) {
			request.putProperty(HANDLER, new LinkedList<>());
		}

		@SuppressWarnings("unchecked")
		LinkedList<EventHandler<?>> handlers = (LinkedList<EventHandler<?>>) request.getProperty(HANDLER);

		handlers.addFirst(handler);
	}

	
	public static List<EventHandler<?>> handlers(EventRequest request) {
		
		if(null == request) {
			return ImmutableList.of(); 
		}
		
		@SuppressWarnings("unchecked")
		List<EventHandler<?>> handlers = (List<EventHandler<?>>) request.getProperty(HANDLER);

		if (null == handlers) {
			return ImmutableList.of();
		}

		return handlers;
	}

}

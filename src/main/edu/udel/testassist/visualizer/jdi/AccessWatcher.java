package edu.udel.testassist.visualizer.jdi;

import java.util.LinkedList;
import java.util.List;

import com.google.common.base.Predicate;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import com.sun.jdi.Field;
import com.sun.jdi.ObjectReference;
import com.sun.jdi.ReferenceType;
import com.sun.jdi.Type;
import com.sun.jdi.VirtualMachine;
import com.sun.jdi.event.ClassPrepareEvent;
import com.sun.jdi.event.WatchpointEvent;
import com.sun.jdi.request.ClassPrepareRequest;
import com.sun.jdi.request.EventRequest;
import com.sun.jdi.request.EventRequestManager;
import com.sun.jdi.request.WatchpointRequest;

public class AccessWatcher {

	private final EventHandler<ClassPrepareEvent> classPrepareHandler = new EventHandler<ClassPrepareEvent>() {

		@Override
		public void handle(ClassPrepareEvent event) {

			ReferenceType referenceType = event.referenceType();

			if (isRelevant.apply(referenceType)) {
				installWatchpointsForClass(referenceType);
			}
		}
	};
	
	private final EventHandler<WatchpointEvent> handler = new EventHandler<WatchpointEvent>() {
		public void handle(WatchpointEvent event) {
			
			Field field = event.field();

			if (field.isStatic()) {
				events.put(field.declaringType().classObject(), field);
			}
			else {
				ObjectReference object = event.object();

				if (isRelevant.apply(object.referenceType())) {
					object.disableCollection();
					events.put(object, field);
				}
			}
		}
	};

	private final LoadingCache<Field, WatchpointRequest> cache = CacheBuilder.newBuilder()
		.build(new CacheLoader<Field, WatchpointRequest>() {

			public WatchpointRequest load(Field field) {
				WatchpointRequest request = eventRequestManager.createAccessWatchpointRequest(field);
				EventRequests.appendHandler(request, handler);
				request.setSuspendPolicy(EventRequest.SUSPEND_NONE);
				return request;
			}
		});

	private final VirtualMachine virtualMachine;
	protected final EventRequestManager eventRequestManager;
	private final Predicate<Type> isRelevant;

	private final ClassPrepareRequest classPrepareRequest;
	private final List<WatchpointRequest> requests;
	private final Multimap<ObjectReference, Field> events;

	public AccessWatcher(final VirtualMachine virtualMachine, final Predicate<Type> isRelevant) {
		this.virtualMachine = virtualMachine;
		this.eventRequestManager = virtualMachine.eventRequestManager();
		this.isRelevant = isRelevant;

		events = HashMultimap.create();
		requests = new LinkedList<>();

		classPrepareRequest = eventRequestManager.createClassPrepareRequest();
		EventRequests.appendHandler(classPrepareRequest, classPrepareHandler);
	}

	public void start() {

		for (ReferenceType referenceType : Iterables.filter(virtualMachine.allClasses(), isRelevant)) {
			installWatchpointsForClass(referenceType);
		}

		classPrepareRequest.enable();
	}

	public void stop() {
		for (WatchpointRequest request : requests) {
			request.disable();
		}
		requests.clear();
		classPrepareRequest.disable();
	}

	public Multimap<ObjectReference, Field> events() {
		return ImmutableMultimap.copyOf(events);
	}

	private void installWatchpointsForClass(ReferenceType referenceType) {

		for (Field field : referenceType.visibleFields()) {
			
			if(field.name().equals("this$0")) continue;
			WatchpointRequest request = cache.getUnchecked(field);
			request.enable();

			requests.add(request);
		}
	}
}

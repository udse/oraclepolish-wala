package edu.udel.testassist.visualizer.jdi;

import com.google.common.base.Predicate;
import com.sun.jdi.Field;

public class Fields {

	public static final Predicate<Field> IS_STATIC = new Predicate<Field>() {
		public boolean apply(final Field field) {
			return field.isStatic();
		}
	};
	
}

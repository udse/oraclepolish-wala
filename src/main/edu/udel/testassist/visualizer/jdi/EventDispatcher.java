package edu.udel.testassist.visualizer.jdi;

import java.util.List;

import com.google.common.collect.Iterables;
import com.sun.jdi.VMDisconnectedException;
import com.sun.jdi.VirtualMachine;
import com.sun.jdi.event.AccessWatchpointEvent;
import com.sun.jdi.event.BreakpointEvent;
import com.sun.jdi.event.ClassPrepareEvent;
import com.sun.jdi.event.EventSet;
import com.sun.jdi.event.ExceptionEvent;
import com.sun.jdi.event.ModificationWatchpointEvent;
import com.sun.jdi.event.StepEvent;

public class EventDispatcher implements Runnable {

	private final VirtualMachine vm;

	public EventDispatcher(VirtualMachine vm) {
		this.vm = vm;
	}

	@Override
	public void run() {
		while (true) {
			try {
				EventSet events = vm.eventQueue().remove();

				// System.out.println(events);

				for (ClassPrepareEvent event : Iterables.filter(events, ClassPrepareEvent.class)) {
					@SuppressWarnings("unchecked")
					List<EventHandler<ClassPrepareEvent>> handlers = (List) EventRequests.handlers(event.request());

					for (EventHandler<ClassPrepareEvent> handler : handlers) {
						handler.handle(event);
					}
				}

				for (StepEvent event : Iterables.filter(events, StepEvent.class)) {
					@SuppressWarnings("unchecked")
					List<EventHandler<StepEvent>> handlers = (List) EventRequests.handlers(event.request());

					for (EventHandler<StepEvent> handler : handlers) {
						handler.handle(event);
					}
				}

				for (ExceptionEvent event : Iterables.filter(events, ExceptionEvent.class)) {
					@SuppressWarnings("unchecked")
					List<EventHandler<ExceptionEvent>> handlers = (List) EventRequests.handlers(event.request());

					for (EventHandler<ExceptionEvent> handler : handlers) {
						handler.handle(event);
					}
				}
				
				for (BreakpointEvent event : Iterables.filter(events, BreakpointEvent.class)) {
					@SuppressWarnings("unchecked")
					List<EventHandler<BreakpointEvent>> handlers = (List) EventRequests.handlers(event.request());

					for (EventHandler<BreakpointEvent> handler : handlers) {
						handler.handle(event);
					}
				}

				for (AccessWatchpointEvent event : Iterables.filter(events, AccessWatchpointEvent.class)) {
					@SuppressWarnings("unchecked")
					List<EventHandler<AccessWatchpointEvent>> handlers = (List) EventRequests.handlers(event.request());

					for (EventHandler<AccessWatchpointEvent> handler : handlers) {
						handler.handle(event);
					}
				}

				for (ModificationWatchpointEvent event : Iterables.filter(events, ModificationWatchpointEvent.class)) {
					@SuppressWarnings("unchecked")
					List<EventHandler<ModificationWatchpointEvent>> handlers = (List) EventRequests.handlers(event.request());

					for (EventHandler<ModificationWatchpointEvent> handler : handlers) {
						handler.handle(event);
					}
				}

				events.resume();
			}
			catch (VMDisconnectedException e) {
				break;
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}

package edu.udel.testassist.visualizer.jdi;

import java.util.Collection;

import com.sun.jdi.Location;
import com.sun.jdi.Method;

import edu.udel.testassist.visualizer.jdi.bytecode.BytecodeScanner;
import edu.udel.testassist.visualizer.jdi.bytecode.ReturnFinder;

public class Methods {

	public static Collection<Location> returnLocationsOf(final Method method) {
		ReturnFinder finder = new ReturnFinder(method);
		BytecodeScanner scanner = new BytecodeScanner(finder);
		scanner.scan(method.bytecodes());

		return finder.returnLocations();
	}
	
}

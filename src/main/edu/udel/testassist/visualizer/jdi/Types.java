package edu.udel.testassist.visualizer.jdi;

import com.google.common.base.Function;
import com.sun.jdi.Type;

public class Types {

	public static Function<Type, String> NAME = new Function<Type, String>() {
		public String apply(final Type type) {
			return type.name();
		}
	};
	
}

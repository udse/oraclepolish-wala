package edu.udel.testassist.visualizer.jdi;

import com.sun.jdi.event.Event;

public interface EventHandler<T extends Event> {
	
	public abstract void handle(T event) throws Exception;
}
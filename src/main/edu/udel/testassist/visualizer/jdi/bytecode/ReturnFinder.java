package edu.udel.testassist.visualizer.jdi.bytecode;

import java.util.SortedSet;

import com.google.common.collect.ImmutableSortedSet;
import com.sun.jdi.Location;
import com.sun.jdi.Method;

public class ReturnFinder extends BytecodeVisitor {

	private final ImmutableSortedSet.Builder<Location> builder;
	private final Method method;

	public ReturnFinder(final Method method) {
		this.method = method;
		builder = ImmutableSortedSet.naturalOrder();
	}

	protected void ireturn() {
		builder.add(method.locationOfCodeIndex(currentOpcodeOffset()));
	}

	protected void lreturn() {
		builder.add(method.locationOfCodeIndex(currentOpcodeOffset()));
	}

	protected void freturn() {
		builder.add(method.locationOfCodeIndex(currentOpcodeOffset()));
	}

	protected void dreturn() {
		builder.add(method.locationOfCodeIndex(currentOpcodeOffset()));
	}

	protected void areturn() {
		builder.add(method.locationOfCodeIndex(currentOpcodeOffset()));
	}

	protected void vreturn() {
		builder.add(method.locationOfCodeIndex(currentOpcodeOffset()));
	}

	public SortedSet<Location> returnLocations() {
		return builder.build();
	}

}

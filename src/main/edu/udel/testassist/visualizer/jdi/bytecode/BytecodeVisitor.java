package edu.udel.testassist.visualizer.jdi.bytecode;

import edu.udel.testassist.visualizer.jdi.bytecode.BytecodeScanner;
import edu.udel.testassist.visualizer.jdi.bytecode.Opcode;

/**
 * An abstract class that can be extended and paired with a {@link BytecodeScanner} to process the instructions decoded from a JVM instruction stream.
 */
public abstract class BytecodeVisitor {

	private BytecodeScanner bytecodeScanner;

	void setBytecodeScanner(BytecodeScanner bytecodeScanner) {
		this.bytecodeScanner = bytecodeScanner;
	}

	public final BytecodeScanner bytecodeScanner() {
		return bytecodeScanner;
	}

	public final Opcode currentOpcode() {
		return bytecodeScanner.currentOpcode();
	}

	public final int currentOpcodeOffset() {
		return bytecodeScanner.currentOpcodeOffset();
	}

	/**
	 * Gives the byte code visitor an opportunity to do something in the presence of its byte code scanner before the first byte gets scanned.
	 */
	protected void prologue() {
	}

	/**
	 * Subclasses override this method if they need to do any processing after the {@linkplain #bytecodeScanner() scanner} has decoded a complete
	 * instruction.
	 * <p>
	 * The default implementation does nothing.
	 */
	protected void instructionDecoded() {
	}

	protected void nop() {
	}

	protected void aconst_null() {
	}

	protected void iconst_m1() {
	}

	protected void iconst_0() {
	}

	protected void iconst_1() {
	}

	protected void iconst_2() {
	}

	protected void iconst_3() {
	}

	protected void iconst_4() {
	}

	protected void iconst_5() {
	}

	protected void lconst_0() {
	}

	protected void lconst_1() {
	}

	protected void fconst_0() {
	}

	protected void fconst_1() {
	}

	protected void fconst_2() {
	}

	protected void dconst_0() {
	}

	protected void dconst_1() {
	}

	protected void bipush(int operand) {
	}

	protected void sipush(int operand) {
	}

	protected void ldc(int index) {
	}

	protected void ldc_w(int index) {
	}

	protected void ldc2_w(int index) {
	}

	protected void iload(int index) {
	}

	protected void lload(int index) {
	}

	protected void fload(int index) {
	}

	protected void dload(int index) {
	}

	protected void aload(int index) {
	}

	protected void iload_0() {
	}

	protected void iload_1() {
	}

	protected void iload_2() {
	}

	protected void iload_3() {
	}

	protected void lload_0() {
	}

	protected void lload_1() {
	}

	protected void lload_2() {
	}

	protected void lload_3() {
	}

	protected void fload_0() {
	}

	protected void fload_1() {
	}

	protected void fload_2() {
	}

	protected void fload_3() {
	}

	protected void dload_0() {
	}

	protected void dload_1() {
	}

	protected void dload_2() {
	}

	protected void dload_3() {
	}

	protected void aload_0() {
	}

	protected void aload_1() {
	}

	protected void aload_2() {
	}

	protected void aload_3() {
	}

	protected void iaload() {
	}

	protected void laload() {
	}

	protected void faload() {
	}

	protected void daload() {
	}

	protected void aaload() {
	}

	protected void baload() {
	}

	protected void caload() {
	}

	protected void saload() {
	}

	protected void istore(int index) {
	}

	protected void lstore(int index) {
	}

	protected void fstore(int index) {
	}

	protected void dstore(int index) {
	}

	protected void astore(int index) {
	}

	protected void istore_0() {
	}

	protected void istore_1() {
	}

	protected void istore_2() {
	}

	protected void istore_3() {
	}

	protected void lstore_0() {
	}

	protected void lstore_1() {
	}

	protected void lstore_2() {
	}

	protected void lstore_3() {
	}

	protected void fstore_0() {
	}

	protected void fstore_1() {
	}

	protected void fstore_2() {
	}

	protected void fstore_3() {
	}

	protected void dstore_0() {
	}

	protected void dstore_1() {
	}

	protected void dstore_2() {
	}

	protected void dstore_3() {
	}

	protected void astore_0() {
	}

	protected void astore_1() {
	}

	protected void astore_2() {
	}

	protected void astore_3() {
	}

	protected void iastore() {
	}

	protected void lastore() {
	}

	protected void fastore() {
	}

	protected void dastore() {
	}

	protected void aastore() {
	}

	protected void bastore() {
	}

	protected void castore() {
	}

	protected void sastore() {
	}

	protected void pop() {
	}

	protected void pop2() {
	}

	protected void dup() {
	}

	protected void dup_x1() {
	}

	protected void dup_x2() {
	}

	protected void dup2() {
	}

	protected void dup2_x1() {
	}

	protected void dup2_x2() {
	}

	protected void swap() {
	}

	protected void iadd() {
	}

	protected void ladd() {
	}

	protected void fadd() {
	}

	protected void dadd() {
	}

	protected void isub() {
	}

	protected void lsub() {
	}

	protected void fsub() {
	}

	protected void dsub() {
	}

	protected void imul() {
	}

	protected void lmul() {
	}

	protected void fmul() {
	}

	protected void dmul() {
	}

	protected void idiv() {
	}

	protected void ldiv() {
	}

	protected void fdiv() {
	}

	protected void ddiv() {
	}

	protected void irem() {
	}

	protected void lrem() {
	}

	protected void frem() {
	}

	protected void drem() {
	}

	protected void ineg() {
	}

	protected void lneg() {
	}

	protected void fneg() {
	}

	protected void dneg() {
	}

	protected void ishl() {
	}

	protected void lshl() {
	}

	protected void ishr() {
	}

	protected void lshr() {
	}

	protected void iushr() {
	}

	protected void lushr() {
	}

	protected void iand() {
	}

	protected void land() {
	}

	protected void ior() {
	}

	protected void lor() {
	}

	protected void ixor() {
	}

	protected void lxor() {
	}

	protected void iinc(int index, int addend) {
	}

	protected void i2l() {
	}

	protected void i2f() {
	}

	protected void i2d() {
	}

	protected void l2i() {
	}

	protected void l2f() {
	}

	protected void l2d() {
	}

	protected void f2i() {
	}

	protected void f2l() {
	}

	protected void f2d() {
	}

	protected void d2i() {
	}

	protected void d2l() {
	}

	protected void d2f() {
	}

	protected void i2b() {
	}

	protected void i2c() {
	}

	protected void i2s() {
	}

	protected void lcmp() {
	}

	protected void fcmpl() {
	}

	protected void fcmpg() {
	}

	protected void dcmpl() {
	}

	protected void dcmpg() {
	}

	protected void ifeq(int offset) {
	}

	protected void ifne(int offset) {
	}

	protected void iflt(int offset) {
	}

	protected void ifge(int offset) {
	}

	protected void ifgt(int offset) {
	}

	protected void ifle(int offset) {
	}

	protected void if_icmpeq(int offset) {
	}

	protected void if_icmpne(int offset) {
	}

	protected void if_icmplt(int offset) {
	}

	protected void if_icmpge(int offset) {
	}

	protected void if_icmpgt(int offset) {
	}

	protected void if_icmple(int offset) {
	}

	protected void if_acmpeq(int offset) {
	}

	protected void if_acmpne(int offset) {
	}

	protected void goto_(int offset) {
	}

	protected void jsr(int offset) {
	}

	protected void ret(int index) {
	}

	protected void tableswitch(int defaultOffset, int lowMatch, int highMatch, int[] offsets) {
	}

	protected void lookupswitch(int defaultOffset, int[] keys, int[] offsets) {
	}

	protected void ireturn() {
	}

	protected void lreturn() {
	}

	protected void freturn() {
	}

	protected void dreturn() {
	}

	protected void areturn() {
	}

	protected void vreturn() {
	}

	protected void getstatic(int index) {
	}

	protected void putstatic(int index) {
	}

	protected void getfield(int index) {
	}

	protected void putfield(int index) {
	}

	protected void invokevirtual(int index) {
	}

	protected void invokespecial(int index) {
	}

	protected void invokestatic(int index) {
	}

	protected void invokeinterface(int index, int count) {
	}

	protected void new_(int index) {
	}

	protected void newarray(int tag) {
	}

	protected void anewarray(int index) {
	}

	protected void arraylength() {
	}

	protected void athrow() {
	}

	protected void checkcast(int index) {
	}

	protected void instanceof_(int index) {
	}

	protected void monitorenter() {
	}

	protected void monitorexit() {
	}

	protected void wide() {
	}

	protected void multianewarray(int index, int nDimensions) {
	}

	protected void ifnull(int offset) {
	}

	protected void ifnonnull(int offset) {
	}

	protected void goto_w(int offset) {
	}

	protected void jsr_w(int offset) {
	}

}

package edu.udel.testassist.visualizer;

import java.util.Map;

import com.google.common.collect.Multimap;
import com.google.common.util.concurrent.FutureCallback;
import com.sun.jdi.ClassObjectReference;
import com.sun.jdi.Field;
import com.sun.jdi.ObjectReference;
import com.sun.jdi.ReferenceType;

public class ResultChecker implements FutureCallback<TestResult> {

	@Override
	public void onFailure(Throwable throwable) {
		throwable.printStackTrace();
	}

	@Override
	public void onSuccess(TestResult result) {

		Multimap<ReferenceType, ObjectReference> objects = result.objects();
		Multimap<ObjectReference, Field> accesses = result.accesses();
//		Multimap<ObjectReference, Field> modifications = result.modifications();

//		for (Map.Entry<ObjectReference, Field> entry : modifications.entries()) {
//
//			ObjectReference object = entry.getKey();
//
//			if (object instanceof ClassObjectReference)
//				continue;
//
//			Field field = entry.getValue();
//
//			if (!objects.containsValue(object)) {
//				System.out.printf("\t%s.%s modified but not a test object\n",
//						String.format("%s[%d]", object.referenceType().name(), object.uniqueID()),
//						field.name());
//			}
//		}

		for (Map.Entry<ObjectReference, Field> entry : accesses.entries()) {

			ObjectReference object = entry.getKey();

			if (object instanceof ClassObjectReference)
				continue;

			Field field = entry.getValue();

			if (!objects.containsValue(object)) {
				System.out.printf("\t%s.%s accessed but not a test object\n",
						String.format("%s[%d]", object.referenceType().name(), object.uniqueID()),
						field.name());
			}
		}
	}
}

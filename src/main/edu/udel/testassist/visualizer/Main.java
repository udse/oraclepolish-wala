package edu.udel.testassist.visualizer;

import static com.google.common.base.Predicates.and;
import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.contains;
import static com.google.common.base.Predicates.instanceOf;
import static com.google.common.base.Predicates.not;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.sun.jdi.AbsentInformationException;
import com.sun.jdi.ArrayType;
import com.sun.jdi.ReferenceType;
import com.sun.jdi.Type;
import com.sun.jdi.VMDisconnectedException;
import com.sun.jdi.VirtualMachine;
import com.sun.jdi.connect.IllegalConnectorArgumentsException;
import com.sun.jdi.connect.VMStartException;

import edu.udel.testassist.configuration.Configuration;
import edu.udel.testassist.configuration.TestInfo;
import edu.udel.testassist.visualizer.jdi.EventDispatcher;
import edu.udel.testassist.visualizer.jdi.Types;
import edu.udel.testassist.visualizer.jdi.VirtualMachineLauncher;

public class Main {

	private static final List<String> EXCLUDE_PREFIXES = ImmutableList.of(
			"$",
			"java.",
			"javax.",
			"com.sun.",
			"sun.",
			"org.omg.",
			"junit.",
			"org.junit.",
			"com.apple.",
			"com.intellij.",
			"org.netbeans.",
			"edu.udel."
		);

	private static final Pattern EXCLUDE_PATTERN = Pattern.compile(Joiner.on("|").join(
			Iterables.transform(EXCLUDE_PREFIXES, new Function<String, String>() {
				public String apply(String string) {
					return "^" + Pattern.quote(string);
				}
			})));

	@SuppressWarnings("unchecked")
	private static final Predicate<Type> IS_USER_CLASS = and(
			// is a ReferenceType (i.e. Class, Interface, or Array)
			instanceOf(ReferenceType.class),
			// but not an Array
			not(instanceOf(ArrayType.class)),
			// and it's name doesn't start with something it shouldn't
			not(compose(contains(EXCLUDE_PATTERN), Types.NAME))
			);

	public static void main(String[] args) throws IOException, AbsentInformationException, InterruptedException {

		OptionParser optionParser = new OptionParser();

		final OptionSpec<File> configurationOption = optionParser.accepts("configuration")
			.withRequiredArg()
			.ofType(File.class)
			.required();

		final OptionSpec<File> outputDirectoryOption = optionParser.accepts("output")
			.withRequiredArg()
			.ofType(File.class)
			.defaultsTo(new File(System.getProperty("user.dir")));

		final OptionSpec<Integer> timeoutOption = optionParser.accepts("timeout")
			.withRequiredArg()
			.ofType(Integer.class)
			.defaultsTo(2);

		final OptionSpec<Integer> startOption = optionParser.accepts("start")
			.withRequiredArg()
			.ofType(Integer.class)
			.defaultsTo(0);

		final OptionSet options = optionParser.parse(args);

		final File outputDirectory = options.valueOf(outputDirectoryOption);
		outputDirectory.mkdirs();

		ObjectMapper mapper = new ObjectMapper();

		Configuration configuration = mapper.readValue(options.valueOf(configurationOption), Configuration.class);

		final ResultPrinter printer = new ResultPrinter(outputDirectory);
		final ResultSummarizer summarizer = new ResultSummarizer(new FileWriter(new File(outputDirectory, "summary.csv")));
		final ResultChecker checker = new ResultChecker();

		String classpath = Joiner.on(":").join(new ImmutableList.Builder<String>()
			.addAll(configuration.classpath())
			.add("build")
			.add("lib/junit-4.11.jar")
			.build());
        
		List<TestInfo> tests = configuration.tests();

		for (int i = options.valueOf(startOption); i < tests.size(); i++) {

			final TestInfo test = tests.get(i);
			ImmutableList<String> inferiorArgs = ImmutableList.<String> builder()
				.add("-cp")
				.add(classpath)
				.add(SingleTestRunner.class.getName())
				.add(test.identifier().toString())
				.build();

			String vmArgs = Joiner.on(" ").join(inferiorArgs);
            
			VirtualMachine virtualMachine = null;
			try {
				virtualMachine = new VirtualMachineLauncher()
					.setMain(vmArgs)
					.setOptions("-Xms2g -Xmx2g")
					.setOutputStream(new FileOutputStream("stdout"))
					.setErrorStream(new FileOutputStream("stderr"))
					.launchAndSuspend();
			}
			catch (VMStartException | IllegalConnectorArgumentsException | IOException e) {
				e.printStackTrace();
				System.exit(-1);
			}

			Thread dispatchThread = new Thread(new EventDispatcher(virtualMachine));
			dispatchThread.start();

			ResultFuture resultFuture = new ResultFuture(test, IS_USER_CLASS, virtualMachine);

			System.out.printf("%d) %s ... ", i, test.identifier());
			System.out.flush();

			Stopwatch watch = new Stopwatch();
			watch.start();

			virtualMachine.resume();

			try {
				TestResult result = resultFuture.get(options.valueOf(timeoutOption), TimeUnit.MINUTES);
				System.out.printf("done [%s]\n", watch);

				printer.onSuccess(result);
				summarizer.onSuccess(result);
				checker.onSuccess(result);
			}
			catch (ExecutionException e) {
				e.printStackTrace();
			}
			catch (TimeoutException e) {
				System.out.printf("timeout [%s]\n", watch);
			}
			try {
				virtualMachine.exit(-1);
			}
			catch (VMDisconnectedException e) {
				
			}

			dispatchThread.join();
		}

		try {
			summarizer.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}

package edu.udel.testassist.visualizer;

import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;

import com.google.common.base.Predicate;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import com.google.common.collect.SetMultimap;
import com.google.common.util.concurrent.AbstractFuture;
import com.sun.jdi.ClassObjectReference;
import com.sun.jdi.ClassType;
import com.sun.jdi.Field;
import com.sun.jdi.IncompatibleThreadStateException;
import com.sun.jdi.Location;
import com.sun.jdi.Method;
import com.sun.jdi.ObjectReference;
import com.sun.jdi.ReferenceType;
import com.sun.jdi.StackFrame;
import com.sun.jdi.Type;
import com.sun.jdi.VirtualMachine;
import com.sun.jdi.event.BreakpointEvent;
import com.sun.jdi.event.ClassPrepareEvent;
import com.sun.jdi.event.Event;
import com.sun.jdi.event.ExceptionEvent;
import com.sun.jdi.request.BreakpointRequest;
import com.sun.jdi.request.ClassPrepareRequest;
import com.sun.jdi.request.EventRequestManager;
import com.sun.jdi.request.ExceptionRequest;
import com.sun.jdi.request.StepRequest;

import edu.udel.testassist.configuration.Range;
import edu.udel.testassist.configuration.TestInfo;
import edu.udel.testassist.visualizer.jdi.AccessWatcher;
import edu.udel.testassist.visualizer.jdi.EventHandler;
import edu.udel.testassist.visualizer.jdi.EventRequests;
import edu.udel.testassist.visualizer.jdi.Methods;
import edu.udel.testassist.visualizer.jdi.ModificationWatcher;

public class ResultFuture extends AbstractFuture<TestResult> {

	private final TestInfo test;
	private final Predicate<Type> isRelevant;
	private final VirtualMachine virtualMachine;
	private final EventRequestManager eventRequestManager;

	private final AccessWatcher accessWatcher;
	private final ModificationWatcher modificationWatcher;

	private final LoadingCache<Location, BreakpointRequest> cache = CacheBuilder.newBuilder().build(new CacheLoader<Location, BreakpointRequest>() {

		public BreakpointRequest load(Location location) {
			BreakpointRequest request = eventRequestManager.createBreakpointRequest(location);
			return request;
		}
	});

	public ResultFuture(final TestInfo test, final Predicate<Type> isRelevant, final VirtualMachine virtualMachine) {
		super();

		this.test = test;
		this.isRelevant = isRelevant;
		this.virtualMachine = virtualMachine;
		this.eventRequestManager = virtualMachine.eventRequestManager();

		accessWatcher = new AccessWatcher(virtualMachine, isRelevant);
		modificationWatcher = new ModificationWatcher(virtualMachine, isRelevant);

		modificationWatcher.start();

		ClassPrepareRequest classPrepareRequest = eventRequestManager.createClassPrepareRequest();
		classPrepareRequest.addClassFilter(test.className());
		EventRequests.appendHandler(classPrepareRequest, new EventHandler<ClassPrepareEvent>() {

			@Override
			public void handle(ClassPrepareEvent event) throws Exception {

				ClassType cls = (ClassType) event.referenceType();

				final Method method = cls.concreteMethodByName(test.methodName(), test.methodSignature());

				final ExceptionRequest methodExitExceptionRequest = eventRequestManager.createExceptionRequest(null, true, true);
				EventRequests.appendHandler(methodExitExceptionRequest, new EventHandler<ExceptionEvent>() {

					@Override
					public void handle(ExceptionEvent event) throws IncompatibleThreadStateException {

						final Location catchLocation = event.catchLocation();

						List<StackFrame> frames = event.thread().frames();

						int catchIndex = (null == catchLocation) ? Integer.MAX_VALUE : Iterables.indexOf(frames, new Predicate<StackFrame>() {
							public boolean apply(final StackFrame frame) {
								return frame.location().method().equals(catchLocation.method());
							}
						});

						int methodIndex = Iterables.indexOf(frames, new Predicate<StackFrame>() {
							public boolean apply(final StackFrame frame) {
								return frame.location().method().equals(method);
							}
						});

						if (methodIndex < catchIndex) {

							ResultFuture.this.onMethodExit(method);

							methodExitExceptionRequest.disable();
						}
					}
				});

				BreakpointRequest methodEntryRequest = cache.getUnchecked(method.locationOfCodeIndex(0));
				EventRequests.appendHandler(methodEntryRequest, new EventHandler<BreakpointEvent>() {

					@Override
					public void handle(BreakpointEvent event) {

						ResultFuture.this.onMethodEntry(method);

						methodExitExceptionRequest.enable();
					}
				});
				methodEntryRequest.enable();

				for (final Range range : test.ranges()) {

					Location location = method.locationsOfLine(range.line()).get(0);

					BreakpointRequest rangeEntryRequest = cache.getUnchecked(location);
					EventRequests.appendHandler(rangeEntryRequest, new EventHandler<BreakpointEvent>() {

						@Override
						public void handle(BreakpointEvent event) {

							ResultFuture.this.onRangeEntry(range);

							final StepRequest stepRequest = eventRequestManager.createStepRequest(event.thread(), StepRequest.STEP_LINE,
									StepRequest.STEP_OVER);
							stepRequest.addCountFilter(range.numSteps());
							stepRequest.enable();

							final ExceptionRequest exceptionRequest = eventRequestManager.createExceptionRequest(null, true, true);
							exceptionRequest.addThreadFilter(event.thread());
							exceptionRequest.enable();

							EventHandler<Event> rangeExitHandler = new EventHandler<Event>() {
								@Override
								public void handle(Event event) {

									ResultFuture.this.onRangeExit(range);

									eventRequestManager.deleteEventRequest(stepRequest);
									eventRequestManager.deleteEventRequest(exceptionRequest);
								}
							};

							EventRequests.appendHandler(stepRequest, rangeExitHandler);
							EventRequests.appendHandler(exceptionRequest, rangeExitHandler);
						}
					});
					rangeEntryRequest.enable();
				}

				// Setup exit handler
				for (Location location : Methods.returnLocationsOf(method)) {

					BreakpointRequest methodExitBreakpointRequest = cache.getUnchecked(location);
					EventRequests.appendHandler(methodExitBreakpointRequest, new EventHandler<BreakpointEvent>() {

						@Override
						public void handle(BreakpointEvent event) {

							ResultFuture.this.onMethodExit(method);

							methodExitExceptionRequest.disable();
						}
					});
					methodExitBreakpointRequest.enable();
				}
			}
		});
		classPrepareRequest.enable();
	}

	private void onMethodEntry(final Method method) {
//		 for (ReferenceType cls : Iterables.filter(virtualMachine.allClasses(), isRelevant)) {
//		 for(ObjectReference instance : cls.instances(0)) {
//		 instance.disableCollection();
//		 }
//		 }
	}

	private void onMethodExit(final Method method) {

		virtualMachine.suspend();

		modificationWatcher.stop();
		accessWatcher.stop();

		SetMultimap<ReferenceType, ObjectReference> objectsByType = HashMultimap.create();
		for (ReferenceType cls : Iterables.filter(virtualMachine.allClasses(), isRelevant)) {
			objectsByType.putAll(cls, cls.instances(0));
		}

		Multimap<ObjectReference, Field> accesses = accessWatcher.events();
		for (ObjectReference instance : accesses.keySet()) {
			if (instance instanceof ClassObjectReference) {
				objectsByType.put(((ClassObjectReference) instance).reflectedType(), instance);
			}
			else {
				objectsByType.put(instance.referenceType(), instance);
			}
		}

		Multimap<ObjectReference, Field> modifications = modificationWatcher.events();
		
		for (ObjectReference instance : modifications.keySet()) {
			if (instance instanceof ClassObjectReference) {
				objectsByType.put(((ClassObjectReference) instance).reflectedType(), instance);
			}
			else {
				objectsByType.put(instance.referenceType(), instance);
			}
		}

		TestResult result = new TestResult(test.identifier(), objectsByType, modifications, accesses);
		set(result);
	}

	private void onRangeEntry(final Range range) {

		for (ReferenceType cls : Iterables.filter(virtualMachine.allClasses(), isRelevant)) {						
			for (ObjectReference instance : cls.instances(0)) {
				instance.disableCollection();
			}
		}

		modificationWatcher.stop();
		accessWatcher.start();
	}

	private void onRangeExit(final Range range) {
		accessWatcher.stop();
		modificationWatcher.start();
	}
}
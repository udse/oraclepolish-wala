package edu.udel.testassist.configuration;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;



public class Location {
	int offset;
	int size;
	
	@JsonCreator
	public Location(@JsonProperty("offset") int offset, @JsonProperty("size") int size){
		this.offset=offset;
		this.size=size;
	}
	
	public String toString(){
		return String.format("[%s,%s]", offset, size);
	}
}
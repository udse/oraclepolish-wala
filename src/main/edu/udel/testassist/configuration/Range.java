package edu.udel.testassist.configuration;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Range {

	private final int line;
	private final int pattern;
	private final String excep;

	@JsonCreator
	public Range(@JsonProperty("line") int line, 
				@JsonProperty("pattern") int pattern){
		this.line = line;
		this.pattern = pattern;
		this.excep = "";
	}

	@JsonCreator
	public Range(@JsonProperty("line") int line, 
				@JsonProperty("pattern") int pattern,
				@JsonProperty("excep") String excep) {
		this.line = line;
		this.pattern = pattern;
		this.excep = excep;
	}

	@JsonGetter
	public int line() {
		return line;
	}

	@JsonGetter
	public int pattern() {
		return pattern;
	}
	
	@JsonGetter
	public String excep() {
		return excep;
	}
	
	@Override
	public String toString() {
		return String.format("[%d, %d, %s]", line, pattern, excep);
	}
	
	@Override
	public boolean equals(Object dat){
		if (dat instanceof Range){
			Range datrange = (Range) dat;
			return this.line == datrange.line; 
		}
		return false;
	}
	
	@Override
	public int hashCode(){
		return this.line;
	}
	
    public boolean inRange(int i){
        return line == i;
}

}

package edu.udel.testassist.configuration;

import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableSet;

public class TestInfo {

	private final MethodIdentifier methodIdentifier;
	private final ImmutableSet<Range> ranges;
	private final ImmutableSet<TaintInfo> taints;
	private final ImmutableSet<TaintInfo> setups;
	
	@JsonCreator
	public TestInfo(@JsonProperty("identifier") final String identifier,
			@JsonProperty("ranges") final Collection<Range> ranges,
			@JsonProperty("setups") final Collection<TaintInfo> setups,
			@JsonProperty("taints") final Collection<TaintInfo> taints) {
		this.methodIdentifier = new MethodIdentifier(identifier);
		this.ranges = ImmutableSet.copyOf(ranges);
		if (setups != null){
			this.setups = ImmutableSet.copyOf(setups);
		}
		else {
			this.setups = null;
		}
		this.taints = ImmutableSet.copyOf(taints);
	}

	public String className() {
		return methodIdentifier.className();
	}

	public String methodName() {
		return methodIdentifier.methodName();
	}

	public String methodSignature() {
		return methodIdentifier.methodSignature();
	}

	public MethodIdentifier identifier() {
		return methodIdentifier;
	}
	
	@JsonGetter
	public Collection<TaintInfo> setups(){
		return setups;
	}
	
	@JsonGetter("identifier")
	private String jsonIdentifier() {
		return methodIdentifier.toString();
	}

	@JsonGetter
	public Collection<Range> ranges() {
		return ranges;
	}
	
	@JsonGetter 
	public Collection<TaintInfo> taints() {
		return taints;
	}

	@Override
	public String toString() {
		return String.format("[%s: %s, %s, %s]", methodIdentifier, ranges, setups, taints);
	}
}
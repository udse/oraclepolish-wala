package edu.udel.testassist.configuration;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Joiner;

public class Configuration {

	private List<String> classpath;
	private List<TestInfo> tests;

	@JsonCreator
	public Configuration(@JsonProperty("classpath") final List<String> classpath,
			@JsonProperty("tests") final List<TestInfo> tests) {
		this.classpath = classpath;
		this.tests = tests;
	}

	@JsonGetter
	public List<String> classpath() {
		return classpath;
	}

	@JsonGetter
	public List<TestInfo> tests() {
		return tests;
	}

	@Override
	public String toString() {
		return String.format("classpath=%s\ntests=%s",
				Joiner.on(":").join(classpath),
				Joiner.on("\n\t").join(tests));
	}
}

package edu.udel.testassist.visitors;

import java.util.HashMap;
import static com.google.common.base.Predicates.not;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Iterables;
import com.ibm.wala.classLoader.IBytecodeMethod;
import com.ibm.wala.ipa.cfg.ExceptionPrunedCFG;
import com.ibm.wala.ipa.cfg.PrunedCFG;
import com.ibm.wala.ipa.cha.ClassHierarchy;
import com.ibm.wala.ssa.DefUse;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.ISSABasicBlock;
import com.ibm.wala.ssa.SSACFG;
import com.ibm.wala.ssa.SSAConditionalBranchInstruction;
import com.ibm.wala.ssa.SSAGetCaughtExceptionInstruction;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAPhiInstruction;
import com.ibm.wala.ssa.SSAPutInstruction;
import com.ibm.wala.ssa.SymbolTable;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.util.graph.dominators.Dominators;
import com.ibm.wala.util.graph.impl.InvertedGraph;

import edu.udel.testassist.analyzer.AbstractAnalyzer;
import edu.udel.testassist.configuration.TaintInfo;

public class ControlFlowAnalyzer extends AbstractAnalyzer {
	protected Dominators<ISSABasicBlock> postDominators;
	protected Dominators<ISSABasicBlock> dominators;
	protected Map<Integer, List<Integer>> map = new HashMap<Integer, List<Integer>>();
	private boolean isAnalyzable = true;
	private SymbolTable symbolTable;
	private TypeReference klass;
	protected ClassHierarchy cha;
	private IR ir;

	private Set<TaintInfo> taints = new HashSet<TaintInfo>();

	public ControlFlowAnalyzer(SSACFG cfg, List<SSAInstruction> insns, IBytecodeMethod method,
			DefUse du, SymbolTable table, ClassHierarchy cha, IR ir) {
		super(cfg, insns, method, du);
		PrunedCFG<SSAInstruction, ISSABasicBlock> prunedCFG = ExceptionPrunedCFG.make(cfg);
		InvertedGraph<ISSABasicBlock> inverted = new InvertedGraph<ISSABasicBlock>(prunedCFG);
		this.symbolTable = table;
		klass = method.getDeclaringClass().getReference();
		this.cha = cha;
		this.ir = ir;
		if (inverted.getNumberOfNodes() == 0) {
			isAnalyzable = false;
		}
		else {
			isAnalyzable = true;
			this.dominators = Dominators.make(prunedCFG, prunedCFG.entry());
			this.postDominators = Dominators.make(inverted, prunedCFG.exit());
			//			if (method.getSignature().contains("SudokuUtils")) {
			//				System.out.println(method);
			//				System.out.println(prunedCFG);
			//			}
		}
	}

	@Override
	public void visitConditionalBranch(SSAConditionalBranchInstruction condIns) {
		if (!isAnalyzable) return;
		int index = insns.indexOf(condIns);
		final ISSABasicBlock condBbl = cfg.getBlockForInstruction(index);
		ISSABasicBlock ipdom = postDominators.getIdom(condBbl);

		if (ipdom != null)
			if (ipdom.isExitBlock()) {
				final List<ISSABasicBlock> preds = FluentIterable.from(cfg.getNormalPredecessors(ipdom))
						.filter(new Predicate<ISSABasicBlock>() {
							public boolean apply(ISSABasicBlock bbl) {
								try {
									return dominators.isDominatedBy(bbl, condBbl);
								}
								catch (Exception e) {
									return false;
								}
							}
						}
						).toList();

				List<ISSABasicBlock> succ = FluentIterable.from(cfg.getNormalSuccessors(condBbl))
						.filter(new Predicate<ISSABasicBlock>() {
							public boolean apply(final ISSABasicBlock bbl) {
								return !Iterables.any(preds,
										new Predicate<ISSABasicBlock>() {
											public boolean apply(ISSABasicBlock pred) {
												try {
													return dominators.isDominatedBy(bbl, pred);
												}
												catch (Exception e) {
													return false;
												}
											}
										});
							}
						}).toList();

				//				for (ISSABasicBlock bbl : preds)
				//					updateMap(bbl, condBbl, condIns, true);

			for (ISSABasicBlock bbl : succ)
				updateMap(bbl, condBbl, condIns, false);
		}
			else {
				updateMap(ipdom, condBbl, condIns, false);
			}

	}

	private void updateMap(ISSABasicBlock ipdom, ISSABasicBlock condBbl,
			SSAInstruction condIns, boolean reverse) {
		
		
		Iterator<SSAInstruction> piter = ipdom.iterator();

		if (piter.hasNext()) {
			SSAInstruction first;

			if (reverse)
				first = Iterables.getLast(ipdom); //should be a return
			else {
				first = Iterables.getFirst(ipdom, null);
			}

			if (first instanceof SSAPhiInstruction) {
				SSAPhiInstruction phi = (SSAPhiInstruction) first;
				Iterator<SSAInstruction> iter = du.getUses(phi.getDef());
				if (iter.hasNext()) first = iter.next();
			}

			if (first instanceof SSAGetCaughtExceptionInstruction) {
				piter.next();
				if (piter.hasNext()) first = piter.next();
			}

			Optional<Integer> bcoOfCond = bytecodeIndexOf(condIns);
			Optional<Integer> bcoOfIPom = bytecodeIndexOf(first);

			if (bcoOfCond.isPresent() && bcoOfIPom.isPresent())
				if (reverse) {
					put(-1 * bcoOfIPom.get(), bcoOfCond.get());
				}
				else
					put(bcoOfIPom.get(), bcoOfCond.get());
		}
	}

	private void put(Integer key, Integer value) {
		if (!map.containsKey(key)) {
			map.put(key, new ArrayList<Integer>());
		}
		map.get(key).add(value);
	}

	public Map<Integer, List<Integer>> getMap() {
		//		if (method.toString().contains("EqualsBuilder, append(Ljava")) {
		//			System.out.println(ir);
		//			System.out.println(method.toString() + map);
		//		}
		return this.map;
	}

}
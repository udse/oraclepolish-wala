package edu.udel.testassist.visitors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.primitives.Ints;
import com.ibm.wala.classLoader.IBytecodeMethod;
import com.ibm.wala.ipa.cfg.ExceptionPrunedCFG;
import com.ibm.wala.ipa.cfg.PrunedCFG;
import com.ibm.wala.ssa.DefUse;
import com.ibm.wala.ssa.ISSABasicBlock;
import com.ibm.wala.ssa.SSACFG;
import com.ibm.wala.ssa.SSACFG.BasicBlock;
import com.ibm.wala.ssa.SSAConditionalBranchInstruction;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.ssa.SSAPhiInstruction;
import com.ibm.wala.ssa.SymbolTable;
import com.ibm.wala.types.MethodReference;
import com.ibm.wala.util.graph.dominators.Dominators;
import com.ibm.wala.util.strings.StringStuff;

import edu.udel.testassist.analyzer.AbstractAnalyzer;
import edu.udel.testassist.configuration.Range;

public class DataFlowAnalyzer extends AbstractAnalyzer {

	private Set<Range> ranges = new HashSet<>();
	private SymbolTable symbolTable;
	private Dominators<ISSABasicBlock> dominators;

	public DataFlowAnalyzer(SSACFG cfg, List<SSAInstruction> insns,
			IBytecodeMethod method, DefUse du, SymbolTable table) {
		super(cfg, insns, method, du);

		this.symbolTable = table;
		this.dominators = Dominators.make(cfg, cfg.entry());
	}

	public Set<Range> getOracleRanges() {
		return this.ranges;
	}

	@Override
	public void visitInvoke(SSAInvokeInstruction instruction) {
		String targetName = instruction.getDeclaredTarget().getName().toString();
		int index = insns.indexOf(instruction);
		final ISSABasicBlock currentBbl = cfg.getBlockForInstruction(index);

		if (targetName.startsWith("assert")) {
			ISSABasicBlock catchBbl = Iterables.find(cfg,
					new Predicate<ISSABasicBlock>() {
						public boolean apply(ISSABasicBlock bbl) {
							return bbl.isCatchBlock() &&
									dominators.isDominatedBy(currentBbl, bbl);
						}
					}, null);

			Optional<Integer> bco = bytecodeIndexOf(instruction);
			if (bco.isPresent()) {
				if (catchBbl == null) 
					ranges.add(new Range(bco.get(), 1));
//				else
//					ranges.add(new Range(bco.get(), -1));
			}
		}
		else if (targetName.startsWith("fail")) {
			Optional<Integer> lineNumber = lineNumberOf(instruction);
			if (lineNumber.isPresent()) {
//				ranges.add(new Range(lineNumber.get(), -2));
			}
		}
	}

	protected Optional<Integer> lineNumberOf(final SSAInstruction insn) {
		Optional<Integer> bco = bytecodeIndexOf(insn);
		if (bco.isPresent()) {
			return Optional.fromNullable(method.getLineNumber(bco.get()));
		}
		else {
			return Optional.absent();
		}
	}

	protected String locationOf(final SSAInstruction insn) {
		return String.format("%s:%d", StringStuff.jvmToReadableType
				(method.getDeclaringClass().getName().toString()), bytecodeIndexOf(insn).or(-1));
	}

}

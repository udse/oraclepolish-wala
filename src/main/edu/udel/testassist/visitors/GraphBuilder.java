package edu.udel.testassist.visitors;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;

import com.ibm.wala.classLoader.CallSiteReference;
import com.ibm.wala.classLoader.IBytecodeMethod;
import com.ibm.wala.ssa.DefUse;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAArrayStoreInstruction;
import com.ibm.wala.ssa.SSABinaryOpInstruction;
import com.ibm.wala.ssa.SSACFG;
import com.ibm.wala.ssa.SSAGetInstruction;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.ssa.SSAPutInstruction;
import com.ibm.wala.ssa.SymbolTable;
import com.ibm.wala.types.FieldReference;

import edu.udel.testassist.analyzer.AbstractAnalyzer;
import edu.udel.testassist.analyzer.utils.FieldNode;
import edu.udel.testassist.analyzer.utils.TAEdge;
import edu.udel.testassist.analyzer.utils.TANode;
import edu.udel.testassist.analyzer.utils.VariableNode;

public class GraphBuilder extends AbstractAnalyzer {

	private DirectedGraph<TANode, TAEdge> dg =
			new DefaultDirectedGraph<TANode, TAEdge>(TAEdge.class);

	public GraphBuilder(SSACFG cfg, List<SSAInstruction> insns,
			final IR ir, IBytecodeMethod method, DefUse du) {
		super(cfg, insns, method, du);
	}

	private boolean addEdge(TANode tapNode, TANode sinkNode) {
		if (tapNode.equals(sinkNode)) return false;

		return dg.addEdge(tapNode, sinkNode, new TAEdge(tapNode, sinkNode));
	}

	@Override
	public void visitArrayStore(SSAArrayStoreInstruction astore) {
	}

	@Override
	public void visitBinaryOp(SSABinaryOpInstruction binary) {
	}

	@Override
	public void visitInvoke(SSAInvokeInstruction invoke) {
		TANode sinkNode;
		TANode tapNode;

		if (invoke.hasDef())
			sinkNode = new VariableNode(invoke.getDef());
		else
			sinkNode = new VariableNode(invoke.getUse(0));

		int start = invoke.hasDef() ? 0 : 1;

		for (int i = start; i < invoke.getNumberOfUses(); i++) {
			int use = invoke.getUse(i);

			tapNode = new VariableNode(use);
			addEdge(tapNode, sinkNode);
		}
	}

	@Override
	public void visitGet(SSAGetInstruction get) {
		FieldReference fref = get.getDeclaredField();
		TANode sinkNode;
		TANode tapNode;
		
		
	}

	@Override
	public void visitPut(SSAPutInstruction put) {
		FieldReference fref = put.getDeclaredField();
		TANode sinkNode;
		TANode tapNode;

		sinkNode = new FieldNode(put.getDef(), fref);
		tapNode = new VariableNode(put.getUse(0));

		addEdge(tapNode, sinkNode);
	}

}

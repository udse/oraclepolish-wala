package edu.udel.testassist.visitors;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.ibm.wala.classLoader.CallSiteReference;
import com.ibm.wala.classLoader.IBytecodeMethod;
import com.ibm.wala.ipa.cha.ClassHierarchy;
import com.ibm.wala.ssa.DefUse;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAArrayStoreInstruction;
import com.ibm.wala.ssa.SSABinaryOpInstruction;
import com.ibm.wala.ssa.SSACFG;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.ssa.SSANewInstruction;
import com.ibm.wala.ssa.SSAPutInstruction;
import com.ibm.wala.ssa.SymbolTable;
import com.ibm.wala.types.MethodReference;
import com.ibm.wala.types.TypeReference;

import edu.udel.testassist.analyzer.AbstractAnalyzer;
import edu.udel.testassist.configuration.TaintInfo;

public class BasicInputAnalyzer extends AbstractAnalyzer {

	private Set<TaintInfo> taints = new HashSet<TaintInfo>();
	private SymbolTable symbolTable;
	private int count;
	private TypeReference klass;
	private ClassHierarchy cha;

	public BasicInputAnalyzer(SSACFG cfg, List<SSAInstruction> insns, final IR ir,
			final IBytecodeMethod method, final DefUse du, final ClassHierarchy ch) {
		super(cfg, insns, method, du);
		this.symbolTable = ir.getSymbolTable();
		System.out.println(method);
		System.out.println(ir);
		klass = method.getDeclaringClass().getReference();
		count = 0;
		this.cha = ch;
	}

	private boolean validInput(int use) {
		return true;
	}

	public Set<TaintInfo> getTaintInfos() {
		return FluentIterable.from(this.taints)
				.filter(new Predicate<TaintInfo>() {
					public boolean apply(TaintInfo ti) {
						return !ti.offsets().isEmpty();
					}
				}).toSet();
	}

	private void storeTaintInfo(SSAInstruction insn, Collection<Integer> offs) {
		Integer bco = bytecodeIndexOf(insn).orNull();
		if (bco != null) taints.add(new TaintInfo(bco, offs));
		count += offs.size();
	}

	public int totalInputs() {
		return count;
	}

	@Override
	public void visitArrayStore(SSAArrayStoreInstruction astore) {
		Collection<Integer> offsets = new HashSet<Integer>();
		int use = astore.getUse(2);
		if (validInput(use))
			offsets.add(symbolTable.isDoubleConstant(use) || symbolTable.isLongConstant(use) ? 1 : 0);

		if (astore.getElementType().isPrimitiveType()) {
			storeTaintInfo(astore, offsets);
		}
		else {
			SSAInstruction def = du.getDef(astore.getUse(0));
			if (def instanceof SSANewInstruction) {
				SSANewInstruction arr = (SSANewInstruction) def;
				if (arr.getConcreteType().getDimensionality() == 1) {
					storeTaintInfo(astore, offsets);
				}
			}
		}
	}

	@Override
	public void visitBinaryOp(SSABinaryOpInstruction binary) {
		Collection<Integer> offsets = new HashSet<Integer>();

		for (int i = binary.getNumberOfUses() - 1; i >= 0; i--) {
			int use = binary.getUse(i);
			if (symbolTable.isConstant(use) || symbolTable.isLongConstant(use)) {
				if (validInput(use)) offsets.add(symbolTable.isConstant(use) ? 1 : 0);
			}
		}

		storeTaintInfo(binary, offsets);
	}

	@Override
	public void visitInvoke(SSAInvokeInstruction invoke) {
		Collection<Integer> offsets = new HashSet<Integer>();
		CallSiteReference csr = invoke.getCallSite();
		
		MethodReference mref = csr.getDeclaredTarget();

		int offset = 0;
		for (int i = mref.getNumberOfParameters() - 1; i >= 0; i--) {
			TypeReference tref = mref.getParameterType(i);
			int size = getTypeSize(tref);
			int use = invoke.getUse(i);

			if (!csr.isStatic()) use = invoke.getUse(i + 1);
			if (symbolTable.isConstant(use) || loadingConstant(use) || loadingMethod(use, klass, cha)) {
				//			if (symbolTable.isConstant(use) || loadingConstant(use)) {
				if (validInput(use)) offsets.add(offset + ((size == 2) ? 1 : 0));
			}

			offset += size;
		}

		if (!mref.getSignature().contains("assert")) storeTaintInfo(invoke, offsets);
	}

	@Override
	public void visitPut(SSAPutInstruction put) {
		Collection<Integer> offsets = new HashSet<Integer>();
		int use = put.getVal();

		if (symbolTable.isConstant(use) || loadingConstant(use) || loadingMethod(use, klass, cha)) {
			TypeReference tref = put.getDeclaredFieldType();
			if (validInput(use)) offsets.add((getTypeSize(tref) == 2) ? 1 : 0);
		}

		storeTaintInfo(put, offsets);
	}

}
